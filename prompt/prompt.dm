/*

This library wraps the built-in global alert() and input() functions in a
/prompt object and related enumerations. They don't get much love because
custom popups are a lot more customizable, but this is how the built-in procs
should have been from the start.

This is an object-oriented programming language, after all.

A /prompt can be instantiated with a timeout, specified in units of deciseconds
(Timeout = 10 = 1 second), which deletes it after that amount of server-time
passes. When the prompt is deleted, all popups displayed to clients by that
prompt are automatically hidden, and any popup procs waiting for input will
return the default value.

Datum: /prompt

	Constructor:
		Format:
			new /prompt // no timeout
			new /prompt (Timeout)

		If Timeout is provided, and less than 1#INF (infinity), the prompt will
		automatically be deleted after [Timeout * 10] seconds, closing all
		prompts that are still open, and returning null for popup procs that
		were called on this object. 

	Proc: ChooseButton

		Format:
			var option = some_prompt.ChooseButton(
				client/Client,
				Message = "" as text,
				Title = "" as text,
				Option1 = "OK" as text,
				[Option2 as text],
				[Option3 as text]
			)

		Parameters:
			Client: the client being shown the popup window.
			Message: shown in the popup window.
			Title: the text in the popup window's title bar.
			Option1, Option2, Option3: options to choose from.
				If no options are provided, "Ok" is shown.

		Returns:
			* Option1, Option2, or Option3.
			* null, if the prompt times out.

		Displays a popup window with 1-3 options to choose from.
		Halts code execution until a response is given.

	Proc: ChooseFromList

		Format:
			var option = some_prompt.ChooseFromList(
				client/Client,
				Message = "" as text,
				Title = "" as text,
				Default in List,
				List[],
				IsOptional = FALSE,
				GetAssociatedValue = FALSE,
				ListFilters = some_prompt.ListFilters.None
			)

		Returns:
			* User's response, as an item in the list, or the value associated
			to the chosen item in the list, if GetAssociatedValue is TRUE.
			* Default, if Client is not a client.
			* null, if the prompt times out, is canceled, or the user
			disconnects.

		Displays a popup box to Client with a vertical list of options.
		Halts code execution until a response is given.

		The Client is the client being shown the popup window.

		The Message is shown in the popup window.

		The Title is the text in the popup window's title bar.

		The Default is initially selected in the list.

		Datums in List are displayed as their "name" variable if present,
		otherwise they are displayed as their type.

		If IsOptional is TRUE, a "Cancel" button is available.

		If GetAssociatedValue is TRUE, the chosen item is used as the key in
		List to return the value associated with it.

		ListFilters: Filters a given list into showing only certain types of
		atoms.
			Names:
				None
				IsMob
				IsObj
				IsTurf
				IsArea
				IsMobOrObj
				IsMobOrTurf
				IsMobOrArea
				IsObjOrTurf
				IsObjOrArea
				IsTurfOrArea
				IsMobOrObjOrTurf
				IsMobOrObjOrArea
				IsMobOrTurfOrArea
				IsObjOrTurfOrArea

	Proc: InputValue

		Format:
			var value = some_prompt.InputValue(
				client/Client,
				Message = "" as text,
				Title = "" as text,
				Default as InputType,
				IsOptional = FALSE,
				InputType = some_prompt.InputType.Text
			)

		Returns:
			* User's response, with value type according to InputType.
			* Default, immediately, if Client is not a client.
			* null, if the prompt times out, is canceled, or the user
			disconnects.

		Displays a popup box to Client that depends on InputType.
		Halts code execution until a response is given.

		The Message is shown in the popup window.

		The Title is the text in the popup window's title bar.

		The Default, for Text, Message, and Number, is initially placed in the
		popup window's input field.

		The Default, for Color, is initially selected.

		If IsOptional is TRUE, a "Cancel" button is available.

		InputType: Determines what kind of input to ask for.
			Names:
				Text: a single-line text field.
				Password: a single-line text field masked with asterisks.
				Message: a multi-line text area.
				Number: a single number.
				Icon: an icon file.
				Sound: a sound file.
				File: any type of file.
				Color: a color from a RGB/HSV selector.

*/

#include <Kaiochao\Enum\Enum.dme>

prompt
	ENUM(ListFilters)
		None = 0
		IsMob = 1 << 1
		IsObj = 1 << 2
		IsTurf = 1 << 3
		IsArea = 1 << 4
		IsMobOrObj = IsMob | IsObj
		IsMobOrTurf = IsMob | IsTurf
		IsMobOrArea = IsMob | IsArea
		IsObjOrTurf = IsObj | IsTurf
		IsObjOrArea = IsObj | IsArea
		IsTurfOrArea = IsTurf | IsArea
		IsMobOrObjOrTurf = IsMobOrObj | IsTurf
		IsMobOrObjOrArea = IsMobOrObj | IsArea
		IsMobOrTurfOrArea = IsMob | IsTurfOrArea
		IsObjOrTurfOrArea = IsObj | IsTurfOrArea

	ENUM(InputType)
		Text = 1
		Password = 2
		Message = 3
		Number = 4
		Icon = 5
		Sound = 6
		File = 7
		Color = 8

	New(Timeout = 1#INF)
		if(Timeout <= 0)
			del src
		if(Timeout < 1#INF)
			spawn(Timeout)
				del src

	proc/ChooseButton(
		client/Client,
		Message,
		Title,
		Option1 = "OK",
		Option2,
		Option3
		)
		return alert(Client, Message, Title, Option1, Option2, Option3)

	#define INPUT(TYPE) input(Client, Message, Title, Default) as TYPE
	#define CRASH_UNSUPPORTED CRASH("Unsupported argument type.")
	proc/ChooseFromList(
		client/Client,
		Message,
		Title,
		Default,
		List[],
		IsOptional = FALSE,
		GetAssociatedValue = FALSE,
		ListFilters/ListFilters = ListFilters.None
		)
		if(IsOptional)
			switch(ListFilters)
				if(ListFilters.None) . = INPUT(null | anything) in List
				if(ListFilters.IsMob) . = INPUT(null | mob) in List
				if(ListFilters.IsObj) . = INPUT(null | obj) in List
				if(ListFilters.IsTurf) . = INPUT(null | turf) in List
				if(ListFilters.IsArea) . = INPUT(null | area) in List
				if(ListFilters.IsMobOrObj) . = INPUT(null | mob | obj) in List
				if(ListFilters.IsMobOrTurf) . = INPUT(null | mob | turf) in List
				if(ListFilters.IsMobOrArea) . = INPUT(null | mob | area) in List
				if(ListFilters.IsObjOrTurf) . = INPUT(null | obj | turf) in List
				if(ListFilters.IsObjOrArea) . = INPUT(null | obj | area) in List
				if(ListFilters.IsTurfOrArea) . = INPUT(null | turf | area) in List
				if(ListFilters.IsMobOrObjOrTurf) . = INPUT(null | mob | obj | turf) in List
				if(ListFilters.IsMobOrObjOrArea) . = INPUT(null | mob | obj | area) in List
				if(ListFilters.IsMobOrTurfOrArea) . = INPUT(null | mob | turf | area) in List
				if(ListFilters.IsObjOrTurfOrArea) . = INPUT(null | obj | turf | area) in List
				else CRASH_UNSUPPORTED

		else
			switch(ListFilters)
				if(ListFilters.None) . = INPUT(anything) in List
				if(ListFilters.IsMob) . = INPUT(mob) in List
				if(ListFilters.IsObj) . = INPUT(obj) in List
				if(ListFilters.IsTurf) . = INPUT(turf) in List
				if(ListFilters.IsArea) . = INPUT(area) in List
				if(ListFilters.IsMobOrObj) . = INPUT(mob | obj) in List
				if(ListFilters.IsMobOrTurf) . = INPUT(mob | turf) in List
				if(ListFilters.IsMobOrArea) . = INPUT(mob | area) in List
				if(ListFilters.IsObjOrTurf) . = INPUT(obj | turf) in List
				if(ListFilters.IsObjOrArea) . = INPUT(obj | area) in List
				if(ListFilters.IsTurfOrArea) . = INPUT(turf | area) in List
				if(ListFilters.IsMobOrObjOrTurf) . = INPUT(mob | obj | turf) in List
				if(ListFilters.IsMobOrObjOrArea) . = INPUT(mob | obj | area) in List
				if(ListFilters.IsMobOrTurfOrArea) . = INPUT(mob | turf | area) in List
				if(ListFilters.IsObjOrTurfOrArea) . = INPUT(obj | turf | area) in List
				else CRASH_UNSUPPORTED

		if(GetAssociatedValue)
			return List[.]

	proc/InputValue(
		client/Client,
		Message = "",
		Title = "",
		Default,
		IsOptional = FALSE,
		InputType/InputType = InputType.Text
		)
		if(IsOptional)
			switch(InputType)
				if(InputType.Text) return INPUT(null | text)
				if(InputType.Password) return INPUT(null | password)
				if(InputType.Message) return INPUT(null | message)
				if(InputType.Number) return INPUT(null | num)
				if(InputType.Icon) return INPUT(null | icon)
				if(InputType.Sound) return INPUT(null | sound)
				if(InputType.File) return INPUT(null | file)
				if(InputType.Color) return INPUT(null | color)
				else CRASH_UNSUPPORTED
		else
			switch(InputType)
				if(InputType.Text) return INPUT(text)
				if(InputType.Password) return INPUT(password)
				if(InputType.Message) return INPUT(message)
				if(InputType.Number) return INPUT(num)
				if(InputType.Icon) return INPUT(icon)
				if(InputType.Sound) return INPUT(sound)
				if(InputType.File) return INPUT(file)
				if(InputType.Color) return INPUT(color)
				else CRASH_UNSUPPORTED
	#undef INPUT

mob
	verb/test_default_text()
		var
			prompt/prompt = new (30)
			result = prompt.InputValue(
				Client = client,
				Message = "Message",
				Title = "Title",
				Default = "qwertyuiop",
				InputType = prompt.InputType.Text
			)
		src << "Result: [isnull(result) ? "null" : result]"

	verb/test_buttons()
		var
			prompt/prompt = new (30)
			result = prompt.ChooseButton(client, "Message", "Title", "Ok")
		src << "Result: [isnull(result) ? "null" : result]"

	verb/test_text()
		var
			prompt/prompt = new (30)
			result = prompt.InputValue(client, "Message", "Title")
		src << "Result: [isnull(result) ? "null" : result]"

	verb/test_password()
		var
			prompt/prompt = new (30)
			result = prompt.InputValue(client, "Message", "Title", InputType = prompt.InputType.Password)
		src << "Result: [isnull(result) ? "null" : result]"

	verb/test_optional_file()
		var
			prompt/prompt = new (30)
			result = prompt.InputValue(client, "Message", "Title", IsOptional = TRUE, InputType = prompt.InputType.File)
		src << "Result: [isnull(result) ? "null" : result]"

	verb/test_optional_icon()
		var
			prompt/prompt = new (30)
			result = prompt.InputValue(client, "Message", "Title", IsOptional = TRUE, InputType = prompt.InputType.Icon)
		src << "Result: [isnull(result) ? "null" : result]"

	verb/test_optional_sound()
		var
			prompt/prompt = new (30)
			result = prompt.InputValue(client, "Message", "Title", IsOptional = TRUE, InputType = prompt.InputType.Sound)
		src << "Result: [isnull(result) ? "null" : result]"

	verb/test_optional_color()
		var
			prompt/prompt = new (30)
			result = prompt.InputValue(client, "Message", "Title", 123, TRUE, prompt.InputType.Color)
		src << "Result: [isnull(result) ? "null" : result]"

	verb/test_list()
		var
			prompt/prompt = new (30)
			result = prompt.ChooseFromList(client, "Message", "Title", 123, list(1, 2, 123, 4, 5))
		src << "Result: [isnull(result) ? "null" : result]"

	verb/test_optional_list()
		var
			prompt/prompt = new (30)
			result = prompt.ChooseFromList(client, "Message", "Title", 123, list(1, 2, 123, 4, 5), TRUE)
		src << "Result: [isnull(result) ? "null" : result]"

	verb/test_object_list()
		var
			prompt/prompt = new (30)
			list[] = newlist(
				/obj { name = "Hello world!" },
				/obj { name = "Don't choose me!" },
				/obj { name = "Choose me!" },
				/obj { name = "Foo!" },
				/obj { name = "Bar!" })
			result = prompt.ChooseFromList(client, "Message", "Title", list[3], list)
		src << "Result: [isnull(result) ? "null" : result]"

	verb/test_optional_object_list()
		var
			prompt/prompt = new (30)
			list[] = newlist(
				/obj { name = "Hello world!" },
				/obj { name = "Don't choose me!" },
				/obj { name = "Choose me!" },
				/obj { name = "Foo!" },
				/obj { name = "Bar!" })
			result = prompt.ChooseFromList(client, "Message", "Title", list[3], list, TRUE)
		src << "Result: [isnull(result) ? "null" : result]"

	verb/test_optional_associative_list()
		var
			prompt/prompt = new (30)
			list[] = list(
				A = new /obj { name = "Hello world!" },
				B = new /obj { name = "Don't choose me!" },
				C = new /obj { name = "Choose me!" },
				D = new /obj { name = "Foo!" },
				E = new /obj { name = "Bar!" })
			result = prompt.ChooseFromList(client, "Message", "Title", list[3], list, TRUE, TRUE)
		src << "Result: [isnull(result) ? "null" : result]"

	verb/test_optional_filtered_list()
		var
			prompt/prompt = new (30)
			mob/mob = new

		mob.name = "Can't choose me!"

		var
			list[] = newlist(
				/obj { name = "Hello world!" },
				/obj { name = "Don't choose me!" },
				/obj { name = "Choose me!" },
				/obj { name = "Foo!" },
				/obj { name = "Bar!" }) + mob
			result = prompt.ChooseFromList(client, "Message", "Title", list[3], list, TRUE, FALSE, prompt.ListFilters.IsObj)
		src << "Result: [isnull(result) ? "null" : result]"

	verb/test_optional_filtered_list_of_values()
		var
			prompt/prompt = new (30)
			list[] = list(1, 2, 3, "4", "5", "6", 7, 8, 9)
			result = prompt.ChooseFromList(client, "Message", "Title", list[3], list, TRUE, FALSE, prompt.ListFilters.IsObj)
		src << "Result: [isnull(result) ? "null" : result]"

	verb/test_button_fail()
		var
			prompt/prompt = new (30)
			result = prompt.ChooseButton(null, "Message", "Title")
		src << "Result: [isnull(result) ? "null" : result]"
