/*

	Fast-moving objects! Tile-based version.

	It would be good to use object pooling here, but it's good enough for a
	small demo on how to use the /mouse, /event, and even /clock.

*/

#if !defined(TILE_WIDTH) || !defined(TILE_HEIGHT)
#define TILE_WIDTH 32
#define TILE_HEIGHT 32
#endif


// `t%` of the way from `a` to `b`
proc/lerp(a, b, t) . = a * (1 - t) + b * t

// Get a random float between `a` and `b`
#define randn(a, b) lerp(a, b, rand())


// Display a gun overlay on the player.
obj/gun
	icon_state = "rect"
	color = "black"
	transform = matrix(4/32, 0, 0, 0, 28/32, 32)
	layer = FLOAT_LAYER

mob/player
	overlays = list(/obj/gun)


client
	var
		// Shoot every tick of this clock.
		tmp/clock/shoot_clock

		// Time in deciseconds between shots
		shoot_speed = 1

	New()
		. = ..()
		EVENT_ADD(mouse.OnPressed, src, .proc/StartShooting)
		EVENT_ADD(mouse.OnMoved, src, .proc/UpdateAim)

	proc
		// Turn the player to face the mouse.
		UpdateAim(mouse/Mouse)
			mob.transform = turn(initial(mob.transform), Mouse.GetAngle())

		// Start shooting when the left mouse button is pressed without
		// modifiers.
		StartShooting(mouse/Mouse, Button, Modifier)
			if(!Modifier && Button == MouseButton.Left)
				// Lazy-initialize the shoot-clock.
				EVENT_ADD(shoot_clock, src, .proc/Shoot)
				EVENT_ADD(Mouse.OnReleased, src, .proc/StopShooting)
				EVENT_REMOVE(Mouse.OnPressed, src, .proc/StartShooting)

		// Stop the clock to stop shooting.
		StopShooting(mouse/Mouse, Button, Modifier)
			if(!Modifier && Button == MouseButton.Left)
				EVENT_REMOVE(shoot_clock, src, .proc/Shoot)
				EVENT_ADD(Mouse.OnPressed, src, .proc/StartShooting)
				EVENT_REMOVE(Mouse.OnReleased, src, .proc/StopShooting)

		// Fire a projectile in the direction of the mouse
		// with some slight inaccuracy.
		// This is called by Clock whenever it ticks.
		Shoot(clock/Clock)
			// Make sure subsequent clock-ticks occur based on our shoot_speed.
			Clock.tick_lag = shoot_speed

			// Instantiate a new bullet at the player's position
			// and send it off towards the mouse (with a bit of inaccuracy).
			new /obj/bullet (mob.loc, mouse.GetAngle() + randn(-5, 5))

// Bullets move according to this clock, which ticks once every frame.
var clock/bullet_clock = new

obj/bullet
	// Bullet appearance.
	icon_state = "oval"
	transform = matrix(4/32, 0, -16, 0, 4/32, -16)

	// Let bullets bump into things.
	density = TRUE

	// Pixel movement + animate_movement = death
	animate_movement = FALSE

	var
		// Pixels to move per frame in any direction.
		speed = 16

		// Pixels to move per frame in either axis.
		velocity_x
		velocity_y

		// Absolute pixel position on the map.
		position_x
		position_y

		// Factor that determines how quickly the bullet slows down over time.
		drag = 0.05

		// Deciseconds until the bullet expires.
		lifetime = 10

	New(atom/Loc, Angle)
		// Bullet expires over time.
		spawn(lifetime)
			del src

		// Initialize the pixel position using the center of the current tile.
		position_x = 1 + (Loc.x - 0.5) * TILE_WIDTH
		position_y = 1 + (Loc.y - 0.5) * TILE_HEIGHT

		// Initialize the bullet's velocity vector using the given `Angle`
		// and the speed.
		velocity_x = speed * sin(Angle)
		velocity_y = speed * cos(Angle)

		// Update the bullet over time.
		EVENT_ADD(bullet_clock, src, .proc/Update)
		Update()

	Del()
		// Manually clean up this reference.
		EVENT_REMOVE(bullet_clock, src, .proc/Update)
		..()

	// Allow bullets through other bullets.
	Cross(atom/movable/M)
		. = istype(M, /obj/bullet) || ..()

	// When the bullet bumps into some object
	// create a dust particle and disappear.
	Bump(atom/O)
		new /obj/dust_particle (get_turf(O), pixel_x - 16, pixel_y - 16)
		del src

	proc
		// Called every tick of the Clock
		Update(clock/Clock)
			// Apply velocity to position, changing its... position.
			position_x += velocity_x
			position_y += velocity_y

			// Apply drag to velocity, changing its speed.
			velocity_x *= 1 - drag
			velocity_y *= 1 - drag

			// Classic custom pixel movement calculations here.
			var turf/new_loc = locate(
				1 + (position_x - 1) / TILE_WIDTH,
				1 + (position_y - 1) / TILE_HEIGHT, z)

			// Fell off the map?
			if(!new_loc)
				del src

			// Still on the map...
			else
				// More classic calculations here.
				pixel_x = (position_x - 1) % TILE_WIDTH
				pixel_y = (position_y - 1) % TILE_HEIGHT

				// Move the bullet to the new loc if it's not already there.
				// This takes advantage of built-in collision detection,
				// but, of course, it's only tile-based collision.
				(loc != new_loc) && Move(new_loc)

obj/dust_particle
	icon_state = "oval"
	transform = matrix(1/32, 0, 0, 0, 1/32, 0)
	alpha = 128
	color = "silver"
	layer = FLY_LAYER // particle will render over most things
	mouse_opacity = FALSE // particles are typically mouse-ignorant

	New(Loc, PixelX, PixelY)
		pixel_x = PixelX
		pixel_y = PixelY
		animate(src,
			time = 10,
			easing = CIRCULAR_EASING | EASE_OUT,
			transform = matrix() * (rand(10, 20)/32),
			alpha = 0
			)
