/*

	This is a simple way to implement a basic click-to-move mechanic.

	When you press the right mouse button over the map, the player will move to
	the turf under the mouse pointer using BYOND's built-in pathfinding.

	The client will be shown a marker on the destination for a short time.

*/

// Define the initial appearance of the move_marker.
image/move_marker
	icon = 'shapes.dmi'
	icon_state = "oval"
	color = "silver"
	blend_mode = BLEND_ADD
	layer = OBJ_LAYER

// Efficiently gets the turf at this atom's outer-most position on the map.
#define get_turf(A) get_step(A, 0)

client
	// On initialization of the client,
	New()
		// Call the default behavior first so mouse is initialized.
		. = ..()

		// To enable ClickMove(), we must add it the mouse.OnPressed event.
		EVENT_ADD(mouse.OnPressed, src, .proc/ClickMove)

	// When client provides move input:
	Move()
		// Call the default behavior, which moves the mob.
		. = ..()

		// Cancel pathfinding.
		walk(mob, 0)

	proc
		ClickMove(mouse/Mouse, Button, Modifier)
			// Want ClickMove() to happen on right-click with no modifiers held.
			// It also requires an object to have been clicked.
			if(!Modifier && Button == MouseButton.Right && Mouse.over_object)

				var image/move_marker/move_marker = new

				// Show the move_marker to the client.
				src << move_marker

				move_marker.transform = matrix() * (1/32)
				animate(move_marker,
					time = 10,
					easing = CIRCULAR_EASING | EASE_OUT,
					alpha = 0,
					transform = null)

				// Hide the move_marker after 1 second passes.
				spawn(10) del move_marker

				// Get the turf that the player is moving to.
				var turf/destination = get_turf(Mouse.over_object)

				// Show the move_marker on the desination turf.
				move_marker.loc = destination

				// The mob should move in-sync with glide_size.
				// If you set glide_size to 0,
				// movement will be called every frame.
				var move_lag = 0
				if(mob.glide_size)
					move_lag = world.tick_lag * world.icon_size / mob.glide_size

				// Start the mob's built-in pathfinding toward the turf.
				// 0: The mob should move onto the turf itself.
				walk_to(mob, destination, 0, move_lag)
