/*

	This file contains code that sets up an environment that shows off the
	(optional) features included in the other code files.

*/

world
	fps = 60
	maxx = 50
	maxy = 50
	turf = /turf/default
	mob = /mob/player
	view = 10

	New()
		// Set the starting location for players
		// to a turf in the center of the map.
		var turf/start = locate(maxx / 2, maxy / 2, 1)
		new /turf/checkered/grass (start)
		start.tag = "start"

		// Randomly generate walls and grass.
		for(var/turf/default/default)
			if(prob(40))
				new /turf/checkered/wall (default)
			else
				new /turf/checkered/grass (default)

		// Apply checkered color to checkered tiles.
		for(var/turf/checkered/checkered)
			checkered.color = (checkered.x + checkered.y) % 2 \
			? checkered.even_color \
			: checkered.odd_color

turf
	default
		icon_state = "rect"
		color = "gray"

	checkered
		var even_color = "black"
		var odd_color = "white"

		grass
			icon_state = "rect"
			color = "green"
			layer = BACKGROUND_LAYER
			even_color = rgb(30, 130, 30)
			odd_color = rgb(30, 150, 30)

		wall
			icon_state = "rect"
			color = "red"
			even_color = rgb(150, 60, 60)
			odd_color = rgb(160, 70, 70)
			layer = OBJ_LAYER
			underlays = list(/obj/outline/outer, /obj/outline/inner)
			density = TRUE

// Define the appearance of "outline" underlays.
obj/outline
	var const/OUTLINE_LAYER = TURF_LAYER + 0.1

	inner
		color = "silver"
		transform = matrix(34/32, 0, 0, 0, 34/32, 0)
		layer = OUTLINE_LAYER + 0.1

	outer
		color = "gray"
		transform = matrix(36/32, 0, 0, 0, 36/32, 0)
		layer = OUTLINE_LAYER

mob/player
	// The appearance of the player.
	icon_state = "oval"
	color = rgb(30, 30, 150)
	transform = matrix(24/32, 0, 0, 0, 24/32, 0)
	underlays = list(/obj/outline/inner)

	// The movement characteristics of the player.
	glide_size = 2
	appearance_flags = LONG_GLIDE

	// The starting location of the player.
	Login()
		loc = locate("start")

// Intro text!

client
	screen = newlist(/obj/intro_text)

obj/intro_text
	screen_loc = "1,1"

	// The maptext takes up the bottom half of the screen.
	maptext_width = 672
	maptext_height = 672 / 2

	maptext = {"<text align=center valign=middle><h1>\
		Left-click and hold to shoot\n
		Middle-click and drag to pan the camera\n
		Right-click on a location to move there
	"}

	New()
		animate(src,
			time = 50,
			easing = CUBIC_EASING | EASE_IN,
			alpha = 0)
		spawn(50)
			del src
