/*

	This is a simple way to implement a camera-moving mechanic.

	When you click and drag the middle mouse button with no modifier keys,
	the camera moves in a way that makes it appear as if you're dragging the map
	around.

	When you release the middle mouse button, the camera smoothly returns.

*/

client
	// On initialization, make sure StartCameraPan
	New()
		. = ..()
		EVENT_ADD(mouse.OnPressed, src, .proc/StartCameraPan)

	proc
		// Smoothly return the camera.
		ResetCamera()
			animate(src,
				time = 10,
				easing = CUBIC_EASING,
				pixel_x = 0,
				pixel_y = 0)

		// This is called whenever a mouse button is pressed.
		StartCameraPan(mouse/Mouse, Button, Modifier)
			// Only start panning if the middle mouse button was pressed
			// with no modifier keys.
			if(!Modifier && Button == MouseButton.Middle)
				// Add events to the mouse that handle camera panning.
				EVENT_ADD(mouse.OnMoved, src, .proc/CameraPan)
				EVENT_ADD(mouse.OnReleased, src, .proc/StopCameraPan)

				// Remove this event from the mouse.
				EVENT_REMOVE(mouse.OnPressed, src, .proc/StartCameraPan)

		StopCameraPan(mouse/Mouse, Button, Modifier)
			if(!Modifier && Button == MouseButton.Middle)
				ResetCamera()

				// Remove camera panning events from the mouse.
				EVENT_REMOVE(mouse.OnMoved, src, .proc/CameraPan)
				EVENT_REMOVE(mouse.OnReleased, src, .proc/StopCameraPan)

				// Add the event that lets the mouse start panning.
				EVENT_ADD(mouse.OnPressed, src, .proc/StartCameraPan)

		// Move the camera according to how the mouse moved.
		CameraPan(mouse/Mouse)
			pixel_x = pixel_x - Mouse.move_x
			pixel_y = pixel_y - Mouse.move_y
