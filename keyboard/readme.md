# Keyboard
*by Kaiochao*

[Library URL][Kaiochao.Keyboard]

[Kaiochao.Keyboard]:	http://www.byond.com/developer/Kaiochao/Keyboard
[Kaiochao.Enum]:		http://www.byond.com/developer/Kaiochao/Enum
[Kaiochao.Clock]:		http://www.byond.com/developer/Kaiochao/Clock
[Kaiochao.Event]:		http://www.byond.com/developer/Kaiochao/Event

### Contents

* [Additions](#additions)
* [Examples](#examples)
* [Changes](#changes)

## Additions

In the Object tab of Dream Maker, enable "Show all nodes" at the bottom.
Compile or Update if you haven't already.
Double-click any of the nodes to go to their (latest) definition.
If it's from this library, don't use it unless it has a description.

###### Compiler Flags
* `KEYBOARD_NO_REPEAT`
* `KEYBOARD_NO_HOOKS`

###### Enums

(see [Kaiochao.Enum])

* `KeyAxis`
* `Keys`

###### Datums
* `keyboard`

###### Keyboard variables
* `client/client`
* `clock/repeat_clock` (see [Kaiochao.Clock])
* `repeat_speed`

###### Keyboard events
(see [Kaiochao.Event])

* `OnKeyDown`
* `OnKeyUp`
* `OnKeyRepeat`
* `OnRepeat`

###### Keyboard procs
* `BindKeys`
* `UnbindKeys`
* `ClearBindings`
* `IsKeyDown`
* `GetAxis`
* `OnKeyDown`
* `OnKeyUp`
* `OnKeyRepeat`
* `OnRepeat`

##### KeyboardHooks.dm

These are only included if `KEYBOARD_NO_HOOKS` is not defined.

###### Interfaces
* `keylistener`

###### Keylistener procs
* `OnKeyDown`
* `OnKeyUp`
* `OnKeyRepeat`
* `OnKeyboardRepeat`

###### Client variables
* `keyboard/keyboard`
* `keylistener/keylistener`

###### Client procs
* `OnKeyDown`
* `OnKeyUp`
* `OnKeyRepeat`
* `OnKeyboardRepeat`

## Examples
###### Do something when the spacebar is pressed:

For BYOND 510 without defining `KEYBOARD_NO_HOOKS`

```dm
#include <kaiochao\keyboard\keyboard.dme>

client
	New()
		. = ..()
		keyboard.BindKeys(src, list(Keys.East, Keys.West, Keys.Z, Keys.X))
		keylistener = mob

	OnKeyDown(keyboard/Keyboard, Key)
		src << "Pressed [Keys.ToName(Key)]!"

mob/platformer
	OnKeyDown(keyboard/Keyboard, Key)
		switch(Key)
			if(Keys.Z)
				Jump()
			if(Keys.X)
				Attack()
```
For BYOND 511 and above with `KEYBOARD_NO_HOOKS` defined
```dm
#include <kaiochao\keyboard\keyboard.dme>

client
	var tmp/keyboard/keyboard

	New()
		. = ..()
		keyboard = new
		keyboard.BindKeys(src)
		EVENT_ADD(keyboard.OnKeyDown, src, .proc/OnKeyDown)
		EVENT_ADD(keyboard.OnKeyDown, mob, .proc/OnKeyDown)

	proc
		OnKeyDown(keyboard/Keyboard, Key)
			src << "Pressed [Keys.ToName(Key)]!"

mob/platformer
	proc
		OnKeyDown(keyboard/Keyboard, Key)
			switch(Key)
				if(Keys.Z)
					Jump()
				if(Keys.X)
					Attack()
```

## Changes

#### July 11 2016

* Added support for BYOND 511's "Any" key binding. Only works in 511!  
  511 isn't out yet, so it's currently untested.

* Added `Keys` enum for safe access to key names.  
  Should be everything on the keyboard, but let me know if I missed anything.

* Added `KeyAxis` enum for the possible return values of `keyboard.GetAxis()`.  
  `GetAxis()` is still compatible with the inequality operators.

* Added `client.OnKeyX()` procs and `client.keylistener`.  
  These are called by `keyboard.OnKeyX()`.  
  They call `client.keylistener.OnKeyX()`.
