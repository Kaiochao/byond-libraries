/*

This library lets you track the keyboard state of a client.

Keyboard
	Variables
		client/client
			Client currently tracked by this keyboard.
			Clients can be tracked by multiple keyboards, but a keyboard can only
			track a single client.

		repeat_tick_lag = 0
			Time between repeats.
			Disabled with KEYBOARD_NO_REPEAT.

	Events
		OnKeyDown(keyboard/Keyboard, KeyCode)
			Event called when any bound key is pressed.

		OnKeyUp(keyboard/Keyboard, KeyCode)
			Event called when any bound key is released.

		OnRepeat(keyboard/Keyboard, clock/Clock)
			Fired every tick of repeat_clock while any key is pressed.
			Disabled with KEYBOARD_NO_REPEAT.

		OnKeyRepeat(keyboard/Keyboard, Key, clock/Clock)
			Fired every tick of repeat_clock for every key that is pressed.
			Disabled with KEYBOARD_NO_REPEAT.

	Methods
		IsKeyDown(KeyCode)
			Returns TRUE if Key is pressed; otherwise, returns FALSE.

		GetKeysDown()
			Returns a list of all tracked keys currently being pressed.

		GetAxis(PositiveKeyCode, NegativeKeyCode)
			Returns one of the KeyAxis values:
				Positive if only PositiveKey is pressed,
				Negative if only NegativeKey is pressed,
				None if neither or both are pressed.

		BindKeys(client/Client)
			(Requires BYOND 511 or above)
			Start tracking all trackable keys of Client.

		BindKeys(client/Client, KeyCodes[])
			(For BYOND 510 and below)
			Start tracking all keys in Keys for Client.

		UnbindKeys(KeyCodes[])
			(For BYOND 510 and below)
			Stop tracking all keys in Keys for Client.

		ClearBindings()
			Stop tracking all keys for the current client.

		GetRepeatClock(TickLag = repeat_tick_lag)
			Returns a /clock that runs with tick_lag = TickLag.
			You can override this to use your own global clock.
			Disabled with KEYBOARD_NO_REPEAT.

Preprocessor macros

	#define KEYBOARD_NO_REPEAT
		Removes repeat-related events and procs, including:
			keyboard.
				repeat_clock
				repeat_tick_lag
				OnRepeat
				OnKeyRepeat
				GetRepeatClock()

	#define KEYBOARD_NO_HOOKS
		Removes the default setup for forwarding
		key events to a client and client.keylistener.
		The hooks are convenient for plug-and-play ability,
		but aren't necessary if you set up your own event handlers.

Enumerations

	KeyCode
		All trackable keys, accessible through an enumeration.
		It's best to use this rather than the raw strings.

	KeyAxis
		Values returned by keyboard.GetAxis().

		None: 0
		Positive: 1
		Negative: -1

*/

ENUM(KeyAxis)
	None = 0
	Positive = 1
	Negative = -1

keyboard
	var
		client/client

		event
			OnKeyDown	// (keyboard/Keyboard, KeyCode)
			OnKeyUp		// (keyboard/Keyboard, KeyCode)
			#ifndef KEYBOARD_NO_REPEAT
			OnRepeat	// (keyboard/Keyboard, clock/Clock)
			OnKeyRepeat	// (keyboard/Keyboard, KeyCode)

		clock/repeat_clock
		repeat_tick_lag = 0

		#endif

		is_key_down[]
		keys_down[]

	Topic(Href, HrefList[])
		switch(HrefList["action"])
			if("key-down")
				var key = HrefList["key"]
				is_key_down[key] = TRUE
				if(keys_down) keys_down += key
				else
					keys_down = list(key)
					#ifndef KEYBOARD_NO_REPEAT
					if(!repeat_clock) repeat_clock = GetRepeatClock(repeat_tick_lag)
					EVENT_ADD(repeat_clock.OnTick, src, .proc/OnRepeat)
					#endif
				OnKeyDown(key)

			if("key-up")
				var key = HrefList["key"]
				is_key_down[key] = FALSE
				if(keys_down)
					keys_down -= key
					if(!keys_down.len)
						keys_down = null
						#ifndef KEYBOARD_NO_REPEAT
						EVENT_REMOVE(repeat_clock.OnTick, src, .proc/OnRepeat)
						if(!repeat_clock.OnTick) repeat_clock = null
						#endif
				OnKeyUp(key)

	proc
		#ifndef KEYBOARD_NO_REPEAT
		GetRepeatClock(TickLag) return new /clock (TickLag)
		#endif

		#if DM_VERSION >= 511
		BindKeys(client/Client)
			client = Client
			is_key_down = new

			// Set a macro on any alphanumeric character so inputs don't steal focus.
			winset(client, "key_anti_input_focus_steal", "parent=macro;name=Z")

			// Set macros for "Any" and "Any+UP" to track the state of all keyboard keys.
			winset(client, "key_Any_\ref[src]", "parent=macro;name=Any;\
				command=\"byond://?src=\ref[src];key=\[\[*\]\];action=key-down\"")
			winset(client, "key_up_Any_\ref[src]", "parent=macro;name=Any+UP;\
				command=\"byond://?src=\ref[src];key=\[\[*\]\];action=key-up\"")

		UnbindKeys()
			is_key_down = null
			keys_down = null
			winset(client, "key_Any_\ref[src]", "parent=")
			winset(client, "key_up_Any_\ref[src]", "parent=")
			client = null

		ClearBindings() UnbindKeys()
		#else
		BindKeys(client/Client, KeyCodes[])
			client = Client

			if(!is_key_down) is_key_down = new
			for(var/key in KeyCodes) if(isnull(is_key_down[key]))
				is_key_down[key] = FALSE
				winset(Client, "key_[key]_\ref[src]","parent=macro; name=[key];\
					command=\"byond://?src=\ref[src];key=[key];action=key-down\"")
				winset(Client, "key_up_[key]_\ref[src]","parent=macro; name=[key]+UP;\
					command=\"byond://?src=\ref[src];key=[key];action=key-up\"")

		UnbindKeys(KeyCodes[])
			if(is_key_down)
				for(var/key in KeyCodes)
					winset(client, "key_[key]_\ref[src]", "parent=")
					winset(client, "key_up_[key]_\ref[src]", "parent=")

				is_key_down -= KeyCodes
				is_key_down = is_key_down.len && is_key_down

				if(keys_down)
					keys_down -= KeyCodes
					if(!keys_down.len)
						keys_down = null
						#ifndef KEYBOARD_NO_REPEAT
						EVENT_REMOVE(repeat_clock.OnTick, src, .proc/OnRepeat)
						#endif

		ClearBindings() UnbindKeys(is_key_down)
		#endif

		IsKeyDown(KeyCode) return is_key_down[KeyCode]
		GetKeysDown() return keys_down.Copy()
		GetAxis(PositiveKey, NegativeKey) return is_key_down[PositiveKey] - is_key_down[NegativeKey]
		OnKeyDown(KeyCode) OnKeyDown && OnKeyDown.Fire(src, KeyCode)
		OnKeyUp(KeyCode) OnKeyUp && OnKeyUp.Fire(src, KeyCode)

		#ifndef KEYBOARD_NO_REPEAT
		OnKeyRepeat(KeyCode, clock/RepeatClock) OnKeyRepeat && OnKeyRepeat.Fire(src, KeyCode, RepeatClock)
		OnRepeat(clock/RepeatClock)
			OnRepeat && OnRepeat.Fire(src, RepeatClock)
			for(var/key in keys_down) OnKeyRepeat(key, RepeatClock)
		#endif
