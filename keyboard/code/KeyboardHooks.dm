#ifndef KEYBOARD_NO_HOOKS
/*

Keylistener Interface
	If you're going to set client.keylistener to some object,
	these are the procs that are called by the client that you can
	define for that keylistener object.

	The arguments are filled-in for you, so you can't change them.

	OnKeyDown(client/Client, keyboard/Keyboard, Key)
		Called when `Key` is pressed for the `Keyboard` of `Client`.

	OnKeyUp(client/Client, keyboard/Keyboard, Key)
		Called when `Key` is released for the `Keyboard` of `Client`.

	OnKeyboardRepeat(client/Client, keyboard/Keyboard, clock/RepeatClock)
		Called every tick when any key is pressed.

	OnKeyRepeat(client/Client, keyboard/Keyboard, Key, clock/RepeatClock)
		Called every tick for every key that is being pressed.

Client variables (tmp)
	keyboard/keyboard
		Reference to the keyboard that this client is using.
		You shouldn't use this if you're given a Keyboard argument.
		This prevents the keyboard from being garbage collected.

	keylistener/keylistener
		Reference to a keylistener to forward key events to.

Client methods
	SetupKeyboard()
		Called when a client connects, to initialize the keyboard.
		By default, this initializes the client's keyboard and
		attaches listeners to keyboard events.

		If you're using BYOND 510 or older, you'll still need to call
		`keyboard.BindKeys(src, Keys[])` to provide a list of keys
		that the keyboard should track.

		Otherwise, all trackable keys are tracked (including gamepad buttons).
*/

keylistener
	proc
		OnKeyDown(client/Client, keyboard/Keyboard, KeyCode)
		OnKeyUp(client/Client, keyboard/Keyboard, KeyCode)
		#ifndef KEYBOARD_NO_REPEAT
		OnKeyboardRepeat(client/Client, keyboard/Keyboard, clock/Clock)
		OnKeyRepeat(client/Client, keyboard/Keyboard, KeyCode, clock/Clock)
		#endif

client
	var tmp
		keyboard/keyboard
		keylistener/keylistener

	New()
		SetupKeyboard()
		. = ..()

	proc
		SetupKeyboard()
			keyboard = new

			#if DM_VERSION >= 511
			keyboard.BindKeys(src)
			#endif
			EVENT_ADD(keyboard.OnKeyDown, src, /client/proc/OnKeyDown)
			EVENT_ADD(keyboard.OnKeyUp, src, /client/proc/OnKeyUp)
			#ifndef KEYBOARD_NO_REPEAT
			EVENT_ADD(keyboard.OnRepeat, src, /client/proc/OnKeyboardRepeat)
			EVENT_ADD(keyboard.OnKeyRepeat, src, /client/proc/OnKeyRepeat)
			#endif

		OnKeyDown(keyboard/Keyboard, KeyCode) hascall(keylistener, "OnKeyDown") && keylistener.OnKeyDown(src, Keyboard, KeyCode)
		OnKeyUp(keyboard/Keyboard, KeyCode) hascall(keylistener, "OnKeyUp") && keylistener.OnKeyUp(src, Keyboard, KeyCode)
		#ifndef KEYBOARD_NO_REPEAT
		OnKeyRepeat(keyboard/Keyboard, KeyCode, clock/Clock) hascall(keylistener, "OnKeyRepeat") && keylistener.OnKeyRepeat(src, Keyboard, KeyCode, Clock)
		OnKeyboardRepeat(keyboard/Keyboard, clock/Clock) hascall(keylistener, "OnKeyboardRepeat") && keylistener.OnKeyboardRepeat(src, Keyboard, Clock)
		#endif
#endif