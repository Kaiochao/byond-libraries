#include <kaiochao\shapes\shapes.dme>

world
	fps = 60
	maxx = 50
	maxy = 50
	turf = /turf/checker
	mob = /mob/player
	New()
		var turf/start = locate(maxx / 2, maxy / 2, 1)
		start.tag = "start"

turf/checker
	icon_state = "rect"
	New() color = (x + y) % 2 ? "silver" : "gray"

mob/player
	icon_state = "oval"
	color = "navy"
	step_size = 2
	Login() loc = locate("start")

client
	ENUM(Controls)
		MoveNorth = KeyCode.W
		MoveSouth = KeyCode.S
		MoveEast = KeyCode.D
		MoveWest = KeyCode.A
		Run = KeyCode.Shift

	#ifdef KEYBOARD_NO_HOOKS
	var keyboard/keyboard = new
	#endif

	New()
		. = ..()
		#if DM_VERSION >= 511
		keyboard.BindKeys(src)
		#else
		/*
		Note: This library doesn't clear any bindings you have in your .dmf
		-- including those in the default skin --
		so you're able to move with the arrow keys and/or WASD in this demo,
		which means you can go twice as fast e.g. by holding D and East;
		but if you have a movement cooldown system, it should be fine.
		*/
		keyboard.BindKeys(src, Controls.GetValues())

		// show the differences between Alpha5, Numpad5, and (default binding for) Center
		keyboard.BindKeys(src, list(
			KeyCode.Numpad5, 	// num-lock on
			KeyCode.Center,	// num-lock off
			KeyCode.Alpha5))	// not on the numpad
		#endif

		/* Note: event handlers occur in the order you add them.
		If you add HandleRunKey before PrintKey, you'll see "Started running!"
		before you see "Key: Shift".
		*/

		// when a key is pressed, show what key was pressed
		EVENT_ADD(keyboard.OnKeyDown, src, .proc/PrintKey)

		// check for running
		EVENT_ADD(keyboard.OnKeyDown, src, .proc/HandleRunKey)
		EVENT_ADD(keyboard.OnKeyUp, src, .proc/HandleRunKeyUp)

		#ifndef KEYBOARD_NO_REPEAT
		// handle movement if any keys are pressed
		EVENT_ADD(keyboard.OnRepeat, src, .proc/HandleMovementKeys)
		#endif

	proc
		/*

			Technically, you could do all of this from the client.OnKey*() procs.
			(unless you have KEYBOARD_NO_HOOKS defined)
			However, the point of handling events through /event is to modularize code.

			Each of these procs could be moved to other datums and/or other files;
			all without having to use ..() anywhere.

		*/

		PrintKey(keyboard/Keyboard, Key)
			/* If you're going to output a key, it might be best to use
			Keys.ToName(Key) because then it'll match the enum used
			to bind or check for the key.
			*/
			src << "Key: [KeyCode.ToName(Key)]"

		HandleRunKey(keyboard/Keyboard, Key)
			if(Key == Controls.Run)
				mob.step_size = 3 * initial(mob.step_size)
				src << "Started running!"

		HandleRunKeyUp(keyboard/Keyboard, Key)
			if(Key == Controls.Run)
				mob.step_size = initial(mob.step_size)
				src << "Stopped running"

		HandleMovementKeys(keyboard/Keyboard, clock/Clock)
			// src << "HandleMovementKeys (Time: [Clock.seconds] seconds)"

			// get the combined direction of movement keys currently pressed
			var move_dir = 0
			switch(Keyboard.GetAxis(Controls.MoveEast, Controls.MoveWest))
				if(KeyAxis.Positive) move_dir |= EAST
				if(KeyAxis.Negative) move_dir |= WEST
			switch(Keyboard.GetAxis(Controls.MoveNorth, Controls.MoveSouth))
				if(KeyAxis.Positive) move_dir |= NORTH
				if(KeyAxis.Negative) move_dir |= SOUTH

			// verbose to stick with built-in functions
			switch(move_dir)
				if(NORTH)		North()
				if(SOUTH)		South()
				if(EAST)		East()
				if(WEST)		West()
				if(NORTHEAST)	Northeast()
				if(NORTHWEST)	Northwest()
				if(SOUTHEAST)	Southeast()
				if(SOUTHWEST)	Southwest()
