#include <kaiochao\shapes\shapes.dme>
#include <kaiochao\clock\clock.dme>
#include <kaiochao\directions\directions.dme>
#include <kaiochao\absolutepositions\absolutepositions.dme>

var clock/game_clock = new

world
	fps = 60
	maxx = 25
	maxy = 25
	turf = /turf/checker
	mob = /mob/player

// Checker pattern turfs.
turf/checker
	icon_state = "rect"
	color = "gray"
	New() color = (x + y) % 2 ? "#555" : "#525252"

mob/player
	icon_state = "oval"
	color = "blue"

	var tmp
		// Represents movement input.
		input_move_x = 0
		input_move_y = 0

		// Represents movement input that is smoothed over time.
		input_velocity_x = 0
		input_velocity_y = 0

		// Fastest you can control yourself, in pixels per frame.
		speed
		walk_speed = 5
		run_speed = 10

		// A factor that determines how quickly you go between zero and max_speed.
		speed_lerp = 0.2

	New()
		// Start ticking along with the game clock.
		EVENT_ADD(game_clock.OnTick, src, .proc/Update)
		speed = walk_speed

	Login()
		// Start listening for changes in input state.
		EVENT_ADD(client.OnButton, src, .proc/HandleButton)
		EVENT_ADD(client.OnMouseMove, src, .proc/HandleMouseMove)
		EVENT_ADD(client.OnAnalog2D, src, .proc/HandleAnalog2D)
		..()

	Logout()
		// Who needs garbage collection?
		del src

	proc
		// Debug output of thumb stick input
		HandleAnalog2D(InputHandler/InputHandler, Macro/Macro, X, Y)
			world.log << "[Macro]: [X], [Y]"

		// A movement of the mouse was detected!
		HandleMouseMove(InputHandler/InputHandler, MoveX, MoveY)
			// If the left mouse button is being pressed,
			if(InputHandler.GetButtonState(Macro.MouseLeftButton))
				// Move in the opposite direction of the mouse's movement.
				// This results in the appearance of dragging the world around yourself.
				Translate(-MoveX, -MoveY)

		// A change in button state was detected!
		HandleButton(InputHandler/InputHandler, Macro/Macro, ButtonState/ButtonState)
			if(ButtonState)
				world.log << "Button pressed: [Macro]"
			switch(Macro)
				if(Macro.D) input_move_x = ButtonState - InputHandler.GetButtonState(Macro.A)
				if(Macro.A) input_move_x = InputHandler.GetButtonState(Macro.D) - ButtonState
				if(Macro.W) input_move_y = ButtonState - InputHandler.GetButtonState(Macro.S)
				if(Macro.S) input_move_y = InputHandler.GetButtonState(Macro.W) - ButtonState
				/* The above 4 lines take advantage of the fact that ButtonState is either 0 or 1.
					Consider the expression (A - B).
					If A is 1 and B is 0, it evaluates to  1 because (1 - 0) is 1.
					If A is 0 and B is 1, it evaluates to -1 because (0 - 1) is -1.
					If A is 1 and B is 1, it evaluates to  0 because (1 - 1) is 0.
					If A is 0 and B is 0, it evaluates to  0 because (0 - 0) is 0.
				*/

				if(Macro.Shift) speed = ButtonState ? run_speed : walk_speed

		Update(clock/Clock)
			input_velocity_x = lerp(input_velocity_x, input_move_x * speed, speed_lerp)
			input_velocity_y = lerp(input_velocity_y, input_move_y * speed, speed_lerp)
			/* Some notes about the above two lines:

				[input_move_x * max_speed] is either 0, [-max_speed], or [max_speed],
			because [input_move_x] is either 0, -1, or 1. Math!

				The [lerp()] here is to cause "exponential decay" of some value,
			[input_velocity_x], toward some target value, [input_move_x * max_speed].

				[speed_lerp] is a number between 0 and 1 that determines
			how quickly the value tends toward the target.

				The effect is that the player gradually accelerates and decelerates
			between 0, [-max_speed], and [max_speed], according to movement input.

			*/

			// Move along the velocity vector.
			Translate(input_velocity_x, input_velocity_y)

/*

	This is a proc that comes standard in probably every math library ever.
	lerp stands for linear interpolation.
	It basically gets you the number that is c% of the way from a to b.
	e.g. lerp(0, 100, 0.5) == 50, lerp(100, 200, 0.25) == 125.

*/
proc/lerp(a, b, c) return a * (1 - c) + b * c
