/*
ButtonState is an enumeration that's basically boolean. 
*/
ENUM(ButtonState)
	Released = FALSE
	Pressed = TRUE
