/*
InputHandler is an interface.

This library makes /client implement the InputHandler interface.

Library Flags:
	#define NO_MACRO_OVERWRITE
		This library is designed to be plug-and-play. The default skin has default macros
		which may not be desired, so, by default, this library clears them (at least,
		the ones it knows about; see _InitializeInput() for a list) by setting their
		commands to nothing.
		If you don't want those macros to be cleared, place this define somewhere before
		the line that includes this library (usually in your .dme file).

All you need to know about InputHandlers, which can be directly applied to /client:

InputHandler events:

	OnButton(InputHandler, Macro, ButtonState)
		Fires whenever a button (keyboard, gamepad, or mouse) is pressed or released.
		Macro is one of the Macro enumerations,
			e.g. Macro.W, Macro.Space, Macro.MouseLeftButton, Macro.Control.
			Be careful not to use a MouseModifier when you should be using a Macro.
		ButtonState is one of:
			ButtonState.Released (0)
			ButtonState.Pressed (1)

	OnAnalog2D(InputHandler, Macro, X, Y)
		Fires whenever a gamepad 2D analog input is changed.
		Macro is either Macro.GamepadLeftAnalog or Macro.GamepadRightAnalog.
		X and Y are floating-point numbers;
			typically from -1 to 1, whose magnitude is no more than 1.

	OnMouseWheel(InputHandler, DeltaX, DeltaY, MouseModifiers)
		Fires whenever the mouse wheel is scrolled.
		At the time of this writing, BYOND only supports scrolling in the y-axis.

	OnMouseMove(InputHandler, MoveX, MoveY)
		Fires whenever the mouse moves while being tracked.
		(MoveX, MoveY) is the number of pixels the mouse moved since the last event.

InputHandler public methods:

	GetButtonState(Macro)
		Get the state, as one of the ButtonState enumerations, of any (tracked) button of Macro.

	GetAnalog2DState(Macro)
		Get the state, in the form of list(x, y), of any (tracked) analog stick of Macro.

	GetMouseScreenPosition()
		Get the mouse's pixel position in the screen in the form of list(x, y)
		The bottom-left of the map viewport is list(1, 1).

	GetMouseMapPosition()
		Get the mouse's pixel position on the map.
		The bottom-left pixel of the map is list(1, 1), by tradition.

	CheckModifiers(MouseModifiers)
		Returns TRUE if all the modifiers are pressed.

	TODO (maybe):
		GetMouseInfo()
			Returns an object (maybe associative list) containing:
			* The object currently under the mouse pointer.
			* The last-given "location", "control", "params" arguments of the mouse events.
			* The list2params()-converted params.
			* The last-known screen-loc parts: tile-x, step-x, tile-y, step-y.

		GetMouseDragInfo()
			Returns an object (maybe associative list) containing:
			* The object currently being dragged by the mouse (if any buttons are pressed)
			  or the object that was just dragged and dropped (if no buttons are pressed).
			* The last-given "src_control", "over_control", "src_location", "over_location"
				arguments from MouseDrag().
			This might be useful in OnButton when a mouse button is released.
*/

#if !defined(TILE_WIDTH) || !defined(TILE_HEIGHT)
#error Define TILE_WIDTH and TILE_HEIGHT!
/* You should have TILE_WIDTH and TILE_HEIGHT defined in your .dme
above the BEGIN INCLUDE line so that all included libraries see them.
They should be the number of pixels matching your world.icon_size.
*/
#endif

obj/mouse_catcher
	mouse_opacity = 2
	icon = null
	name = ""
	plane = -100
	layer = BACKGROUND_LAYER
	screen_loc = "SOUTHWEST to NORTHEAST"

InputHandler
	EVENT(OnButton, InputHandler, Macro, ButtonState)
	EVENT(OnAnalog2D, InputHandler, Macro, X, Y)
	EVENT(OnMouseWheel, InputHandler, DeltaX, DeltaY, MouseModifiers)
	EVENT(OnMouseMove, InputHandler, MoveX, MoveY)

	proc
		GetButtonState(MacroID)
		GetAnalog2DState(MacroID)
		GetMouseScreenPosition()
		GetMouseMapPosition()
		CheckModifiers(MouseModifiers)

client
	show_popup_menus = FALSE

	var
		tmp
			_button_state[0]
			_analog2d_state[0]
			_buttons_pressed[0]
			_mouse_screen_position[2]

		global
			obj/mouse_catcher/mouse_catcher = new
			regex/_mouse_screen_loc_regex = regex("^(?:(?!\[0-9]+:)(\\w+):)?(\\d+)(?::(\\d+))?,(\\d+)(?::(\\d+))?$")

	EVENT(OnButton, InputHandler, Macro, ButtonState)
	EVENT(OnAnalog2D, InputHandler, Macro, X, Y)
	EVENT(OnMouseWheel, InputHandler, DeltaX, DeltaY, MouseModifiers)
	EVENT(OnMouseMove, InputHandler, MoveX, MoveY)

	New()
		. = ..()
		_InitializeInput()

	Topic(Href, HrefList[])
		switch(HrefList["action"])
			if("macro-button-down") _MacroButtonDown(HrefList["macro"])
			if("macro-button-up") _MacroButtonUp(HrefList["macro"])
			if("macro-analog2d") _MacroAnalog2D(HrefList["id"], text2num(HrefList["x"]), text2num(HrefList["y"]))

	MouseDown(object, location, control, params)
		_MacroButtonDown(_ExtractMouseButton(params2list(params)))
		..()

	MouseUp(object, location, control, params)
		_MacroButtonUp(_ExtractMouseButton(params2list(params)))
		..()

	MouseWheel(object, delta_x, delta_y, location, control, params)
		OnMouseWheel(src, delta_x, delta_y, _ExtractModifier(params2list(params)))
		..()

	MouseEntered(object, location, control, params)
		_CheckMouseMove(_ExtractScreenPosition(params2list(params)))
		..()

	MouseMove(object, location, control, params)
		_CheckMouseMove(_ExtractScreenPosition(params2list(params)))
		..()

	MouseDrag(src_object, over_object, src_location, over_location, src_control, over_control, params)
		_CheckMouseMove(_ExtractScreenPosition(params2list(params)))
		..()

	verb
		_MacroButtonDown(Macro as text)
			set name = ".macro button down", instant = TRUE
			_buttons_pressed[Macro] = TRUE
			_button_state[Macro] = ButtonState.Pressed
			OnButton(src, Macro, ButtonState.Pressed)

		_MacroButtonUp(Macro as text)
			set name = ".macro button up", instant = FALSE
			_buttons_pressed -= Macro
			_button_state[Macro] = ButtonState.Released
			OnButton(src, Macro, ButtonState.Released)

		_MacroAnalog2D(Macro as text, X as num, Y as num)
			set name = ".macro analog 2d", instant = TRUE
			if(!_analog2d_state[Macro]) _analog2d_state[Macro] = list(0, 0)
			var state[] = _analog2d_state[Macro]
			if(state[1] != X || state[2] != Y)
				state[1] = X
				state[2] = Y
				OnAnalog2D(src, Macro, X, Y)

	proc
		_CheckMouseMove(NewPosition[])
			if(!NewPosition) return
			if(NewPosition[1] != _mouse_screen_position[1] || NewPosition[2] != _mouse_screen_position[2])
				var move_x = NewPosition[1] - _mouse_screen_position[1]
				var move_y = NewPosition[2] - _mouse_screen_position[2]
				_mouse_screen_position = NewPosition
				OnMouseMove(src, move_x, move_y)

		GetButtonState(Macro/Macro) return _button_state[Macro] || 0
		GetAnalog2DState(Macro/Macro) return _analog2d_state[Macro] || list(0, 0)
		GetMouseScreenPosition() return _mouse_screen_position.Copy()

		GetMouseMapPosition()
			. = GetMouseScreenPosition()
			.[1] += bound_x - 1
			.[2] += bound_y - 1

		CheckModifiers(MouseModifiers/MouseModifiers)
			if(MouseModifiers & MouseModifiers.Control && !GetButtonState(Macro.Ctrl)) return FALSE
			if(MouseModifiers & MouseModifiers.Shift && !GetButtonState(Macro.Shift)) return FALSE
			if(MouseModifiers & MouseModifiers.Alt && !GetButtonState(Macro.Alt)) return FALSE
			return TRUE

		// Start tracking macro input. Called automatically in client.New().
		_InitializeInput()
			screen += mouse_catcher

			var id_prefix = "InputHandler\ref[src]"

			#ifndef NO_MACRO_OVERWRITE
			// Overwrite the default macros to do nothing:
			var global/default_macros = list(
				"North+REP", "South+REP", "East+REP", "West+REP",
				"Northeast+REP", "Southeast+REP", "Northwest+REP", "Southwest+REP", "Center+REP",
				"F1", "F1+REP", // Options & Messages
				"F2", "F2+REP", // Screenshot Auto
				"Shift+F2", "Shift+F2+REP", // Screenshot
				// There doesn't seem to be a similar way to intercept Alt+F4.
				)-null
			for(var/macro in default_macros)
				winset(src, "macro_[replacetext(macro, "+", "_")]", "parent=macro; name=[macro]")
			#endif

			// Track the Any key
			winset(src, "[id_prefix]Any_key", "parent=macro; name=Any; command=\".macro-button-down \[\[*]]\"")
			winset(src, "[id_prefix]Any_key_up", "parent=macro; name=Any+UP; command=\".macro-button-up \[\[*]]\"")

			// Track gamepad 2D analog sticks
			winset(src, "[id_prefix]GamepadLeftAnalog", "parent=macro; name=GamepadLeftAnalog; command=\".macro-analog-2d \\\"GamepadLeftAnalog\\\" \[\[x]] \[\[y]]\"")
			winset(src, "[id_prefix]GamepadRightAnalog", "parent=macro; name=GamepadRightAnalog; command=\".macro-analog-2d \\\"GamepadRightAnalog\\\" \[\[x]] \[\[y]]")

		// Get the modifier flag from a mouse params list
		_ExtractModifier(Params[])
			. = 0
			if(Params["ctrl"])	. |= MouseModifiers.Control
			if(Params["shift"])	. |= MouseModifiers.Shift
			if(Params["alt"])	. |= MouseModifiers.Alt

		// Get the mouse button from a mouse params list
		_ExtractMouseButton(Params[])
			if(Params["left"]) return Macro.MouseLeftButton
			if(Params["right"]) return Macro.MouseRightButton
			if(Params["middle"]) return Macro.MouseMiddleButton

		// Get the screen position from a mouse params list
		_ExtractScreenPosition(Params[])
			var screen_loc = Params["screen-loc"]
			if(_mouse_screen_loc_regex.Find(screen_loc))
				var data[] = _mouse_screen_loc_regex.group
				return list(
					text2num(data[3]) + (text2num(data[2]) - 1) * TILE_WIDTH,
					text2num(data[5]) + (text2num(data[4]) - 1) * TILE_HEIGHT)
