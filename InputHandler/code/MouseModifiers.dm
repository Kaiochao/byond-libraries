/*
MouseModifiers is an enumeration type that supports bit flags.
It uses BYOND's built-in mouse modifier constants.
*/

ENUM(MouseModifiers)
	None = 0
	Control = MOUSE_CTRL_KEY
	Shift = MOUSE_SHIFT_KEY
	Alt = MOUSE_ALT_KEY
