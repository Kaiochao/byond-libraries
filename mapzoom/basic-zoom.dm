// This makes this library basically plug and play.
// Set client.map_zoom to FALSE to disable basic map zoom controls.

client
	var tmp/map_zoom/map_zoom = TRUE

	New()
		. = ..()
		if(map_zoom == TRUE)
			EnableMapZoom()

	// This defines the default map zoom behavior.
	proc
		EnableMapZoom()
			map_zoom = new (src)

			// dmf-free zoom macro insertion (parent must be your window's macro ID)
			var a = url_encode("Ctrl+="), b = url_encode("Ctrl+-")
			winset(src, "zoom_in_key", "parent=macro;name=[a];command=\"byond://?zoom_in\"")
			winset(src, "zoom_out_key", "parent=macro;name=[b];command=\"byond://?zoom_out\"")

		MapZoomTopic(Action)
			Action == "zoom_in" ? map_zoom.ZoomIn() : Action == "zoom_out" && map_zoom.ZoomOut()

		MapZoomScroll(DeltaY, Params[])
			DeltaY && Params["ctrl"] && (DeltaY > 0 ? map_zoom.ZoomIn() : map_zoom.ZoomOut())

	// Zoom macro commands
	Topic(href)
		map_zoom && MapZoomTopic(href)
		..()

	#if DM_VERSION >= 508
	// Mouse wheel zooming: hold Ctrl while scrolling
	MouseWheel(object, delta_x, delta_y, location, control, params)
		..()
		map_zoom && MapZoomScroll(delta_y, params2list(params))
	#endif