#include <kaiochao/shapes/shapes.dme>
atom/icon = 'shapes.dmi'

world
	fps = 60
	maxx = 50
	maxy = 50
	view = "21x15"

// yay for randomly colored squares
turf
	icon_state = "rect"
	New() color = rgb(rand(128), rand(128), rand(128))

mob
	icon_state = "oval"
	color = "white"
	step_size = 4

	Login()
		..()
		animate(src, time = 5, loop = -1, alpha = 128)
		animate(time = 5, alpha = 255)