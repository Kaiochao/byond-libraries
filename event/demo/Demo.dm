// Try setting this to 1 if  you know what soft-referencing is!
#define USE_SOFT_REFERENCE 0

#include <kaiochao\shapes\shapes.dme>

world
	fps = 60

	maxx = 11
	maxy = 11

	New()
		// build a map with walls on the edges and a horizontal wall along a door, with a button that opens the door
		for(var/turf/t)
			if(t.x == 1 || t.x == maxx || t.y == 1 ||  t.y == maxy || (t.y == 7 && t.x != 6))
				new /turf/wall (t)
			else
				new /turf/grass (t)

		var obj/door/door = new (locate(6, 7, 1))

		// make a new trigger, typecasted to /triggerable, but instantiated as an /obj/button (polymorphism!)
		var triggerable/door_trigger = new /obj/button (locate(5, 6, 1))

		// polymorphism lets us replace /obj/button with any triggerable type, such as a /obj/lever
		// go ahead and replace /obj/button with /obj/lever and see the difference in-game

		// open the door when the trigger is triggered
		#if USE_SOFT_REFERENCE
		EVENT_ADD(door_trigger.OnTrigger, "\ref[door]", /obj/door/proc/Triggered)
		#else
		EVENT_ADD(door_trigger.OnTrigger, door, /obj/door/proc/Triggered)
		#endif

turf
	wall
		icon_state = "rect"
		color = "maroon"
		density = TRUE

	grass
		icon_state = "rect"
		color = "green"

// All things that can be triggered should "look like" a /triggerable.
// That is, they should have an OnTrigger event.
// Of course, you shouldn't instantiate this type.
// Having this interface allows the door to handle triggers other than buttons.
triggerable
	// It's recommended to annotate the arguments you intend to pass to an event
	// because all event handlers's callbacks will have access to those arguments.
	// It's also recommended to initialize events ASAP so anyone can subcribe at any point.
	#if DM_VERSION >= 511
	EVENT(OnTrigger, triggerable/Trigger)
	#else
	var tmp/event/OnTrigger // (triggerable)
	#endif

obj
	button
		icon_state = "rect"
		transform = matrix(3/4, 0, 0, 0, 3/4, 0)
		color = "silver"

		#if DM_VERSION >= 511
		EVENT(OnTrigger, triggerable/Trigger)
		#else
		var tmp/event/OnTrigger // (triggerable)
		#endif

		Crossed(atom/movable/M)
			if(ismob(M))
				// When you fire an event, you should always pass the sender as the first argument.
				if(OnTrigger && OnTrigger.Fire(src))
					M << "Click! Something happened!"
				else
					M << "Click! Nothing happened..."

	lever
		icon_state = "rect"
		transform = matrix(1/8, 0, 0, 0, 1/4, 12)
		color = "silver"

		// sure, you could do this with inheritance, but just pretend buttons and levers are sufficiently different

		#if DM_VERSION >= 511
		EVENT(OnTrigger, triggerable/Trigger)
		#else
		var event/OnTrigger // (triggerable)
		#endif

		Crossed(atom/movable/M)
			if(ismob(M))
				if(OnTrigger && OnTrigger.Fire(src))
					transform = matrix(1/8, 0, 0, 0, 1/8, 14)
					M << "You pull the lever! Something happened!"
				else
					M << "The lever is jammed!"

	door
		icon_state = "rect"
		color = "olive"
		density = TRUE

		proc
			Triggered(triggerable/Trigger)
				world << "(Door opened)"

				loc = null
				world << "(Door loc set to null)"

				#if USE_SOFT_REFERENCE
				world << "(Door not detached from trigger, but a soft reference was used)"

				#else
				// Without relying on /obj/button, we can safely detach the door from the trigger event.
				// Comment out the EVENT_REMOVE line and the door won't be garbage-collected.
				EVENT_REMOVE_OBJECT(Trigger.OnTrigger, src)
				world << "(Door detached from trigger, should cause garbage collection)"

				// Note: this wouldn't trigger garbage collection if the door was attached to multiple triggers,
				// as all events have strong references to attached event handlers.
				// To avoid memory leaks, you need to explicitly detach event handlers.
				#endif

		Del()
			// So you can see that the door is garbage collected
			world << "(Door deleted, probably garbage collected)"
			..()

mob
	icon_state = "oval"
	color = "olive"
	step_size = 4
	transform = matrix(1/2, 0, 0, 0, 1/2, 0)
	bounds = "9,9 to 24,24"


// events no longer exist or fire when they don't have any handlers
// but you must use the EVENT_() macros for it to work

mob
	#if DM_VERSION >= 511
	EVENT(OnSaid, mob/Sayer, A, B, C)
	#else
	var tmp/event/OnSaid
	#endif

	Login()
		..()
		if(!OnSaid) src << "OnSaid doesn't exist"
		EVENT_ADD(OnSaid, src, .proc/OnSaid)
		if(OnSaid) src << "OnSaid exists"
		say("test")
		EVENT_REMOVE(OnSaid, src, .proc/OnSaid)
		if(!OnSaid) src << "OnSaid doesn't exist"

	verb
		say(t as text)
			world << "<b>[src]</b> [html_encode(t)]"

			#if DM_VERSION >= 511
			OnSaid(src, 1, 2, 3)
			#else
			OnSaid && OnSaid.Fire(src, 1, 2, 3)
			#endif

	#if DM_VERSION >= 511
	OnSaid(mob/Sayer, A, B, C)
	#else
	proc/OnSaid(mob/Sayer, A, B, C)
	#endif
		world.log << "[src].OnSaid([Sayer], [A], [B], [C])"
