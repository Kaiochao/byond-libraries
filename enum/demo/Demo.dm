ENUM(Weekday)
	Sunday		= 1
	Monday		= 2
	Tuesday		= 3
	Wednesday	= 4
	Thursday	= 5
	Friday		= 6
	Saturday	= 7

ENUM(Directions)
	North		= 1
	South		= 2
	East		= 4
	West		= 8
	Northeast	= North | East
	Northwest	= North | West
	Southeast	= South | East
	Southwest	= South | West

ENUM(MathConstant)
	pi			= 3.14159265359
	e			= 2.71828182846

ENUM(DuplicateValue)
	a = 123
	b = 123

obj
	proc
		GetALife()
			// The `State` enum only exists for mobs!
			//return State.Alive

mob
	ENUM(State)
		Alive = 1
		Dead = 2
		Both = Alive | Dead

	var state = State.Alive

	verb
		set_state(new_state in State.GetNames())
			state = State.ToValue(new_state)

		check_state()
			src << State.ToName(state)

		// how to get the value of an enum: just use the name of the enum, then access the var like usual
		check_math_constants()
			src << "pi = [MathConstant.pi]"
			src << "e = [MathConstant.e]"

		// how to get a list of all names under an enum, and then get the associated value
		check_weekday(weekday in Weekday.GetNames())
			src << "[weekday] is the [Weekday.ToValue(weekday)]\th day of the week!"

		// how to convert a value to the name in the enum associated to it
		change_direction(direction in Directions.GetNames())
			dir = Directions.ToValue(direction)
			src << "dir = [Directions.ToName(dir)]"

		// see what happens when you convert a value to a name for an enum with duplicate values
		check_duplicate_value()
			src << "The DuplicateValue enum has names `a` and `b` which are both associated to the value `123`."
			src << "Since `b` is defined after `a`, the result of `DuplicateValue.ToName(123)` is..."
			src << DuplicateValue.ToName(123)
