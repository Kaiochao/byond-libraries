/*

Preprocessor macros
	ENUM(NAME)
		Define a new enumeration called NAME.
		Creates a global variable NAME referring to a new instance of the /enum type.
		Begins a var/const block, so you can start adding `Name = Value` pairs.

Enumeration methods
	GetNames()
		Returns an array of all names under this enumeration.

	GetValues()
		Returns a set of all available values.
		Set, as in, it's a collection that contains no duplicates.

	ToName(Value)
		Returns the last name associated to a given value.
		Last, as in, if multiple names are associated to the same value,
		then the name of the last one defined (in compilation order) is returned.

	ToValue(Name)
		Returns the value associated to a given name.

*/

#define ENUM(E) var/global/E/E = new; /E/parent_type = /enum; /E/var/const

enum
	var
		global/enum/base_enum = new
		_names[]
		_values[]
		_name_to_value[]
		_value_to_name[]

	New()
		if(type == /enum) return
		_name_to_value = GetNames()
		_value_to_name = new
		for(var/name in _name_to_value)
			var value = vars[name]
			_name_to_value[name] = value
			_value_to_name["[value]"] = name

	proc
		GetNames()
			if(!_names)
				_names = new
				for(var/v in vars - base_enum.vars) _names |= v
			return _names

		GetValues()
			if(!_values)
				_values = new
				for(var/name in GetNames()) _values |= vars[name]
			return _values

		ToName(Value) return _value_to_name["[Value]"]

		ToValue(Name) return _name_to_value[Name]
