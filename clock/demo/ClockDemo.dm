
world
	fps = 60
	loop_checks = FALSE

//var clock/game_clock = world
var clock/game_clock = new
var clock/seconds_clock = new (10)
var clock/five_seconds_clock = new (50)

mob
	Login()
		EVENT_ADD(seconds_clock.OnTick, src, .proc/OneSecondPassed)
		EVENT_ADD(five_seconds_clock.OnTick, src, .proc/FiveSecondsPassed)

	Stat()
		stat("world.time", num2text(world.time))
		stat("Time", num2text(game_clock.time))

		stat("world.tick_lag", num2text(world.tick_lag))
		stat("Tick Lag", num2text(game_clock.tick_lag))

		if(istype(game_clock))
			stat("Time", "[game_clock.time] (off by [num2text(world.time - game_clock.time, 100)])")
			stat("Ticks", game_clock.ticks)
			stat("Milliseconds", game_clock.milliseconds)
			stat("Seconds", game_clock.seconds)

	// even world.time will pause until this finishes (so will all clocks)
	verb/high_cpu()
		var a
		for(var/n in 1 to 1e7) a = sqrt(n)
		for(var/n in 1 to 1e7) a = sqrt(n)
		src << a

	verb/pause()
		src << "Paused!"
		game_clock.Pause()
		seconds_clock.Pause()
		five_seconds_clock.Pause()

	verb/resume()
		src << "Resume!"
		game_clock.Resume()
		seconds_clock.Resume()
		five_seconds_clock.Resume()

	proc/OneSecondPassed(clock/Clock)
		src << "One second passed!"

	proc/FiveSecondsPassed(clock/Clock)
		world << "Five seconds passed!"
