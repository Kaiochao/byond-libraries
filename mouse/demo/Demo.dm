#include <kaiochao\shapes\shapes.dme>

world
	fps = 60
	maxx = 50
	maxy = 50
	turf = /turf/checkerboard
	mob = /mob/player

turf/checkerboard
	icon_state = "rect"
	New()
		var c = x % 2 == y % 2 ? 120 : 130
		color = rgb(c, c, c)

		if(prob(10)) new /obj/clickable_box (src)

obj/clickable_box
	icon_state = "rect"
	color = "gold"
	transform = matrix(3/4, 0, 0, 0, 3/4, 0)

	// non-event-based overrideables that you can use to avoid using usr anywhere ever

	OnMousePressed(mouse/Mouse, Button, Modifier)
		Mouse.client << "[src].OnMousePressed(Mouse, [Button], [Modifier])"

	OnMouseReleased(mouse/Mouse, Button, Modifier)
		Mouse.client << "[src].OnMouseReleased(Mouse, [Button], [Modifier])"

	OnMouseClicked(mouse/Mouse, Button, Modifier)
		Mouse.client << "[src].OnMouseClicked(Mouse, [Button], [Modifier])"

	OnDragAndDrop(mouse/Mouse, Button, Modifier)
		Mouse.client << "[src].OnDragAndDrop(Mouse, [Button], [Modifier]), over_object = [Mouse.over_object]"

client
	#ifndef MOUSE_DISABLE_MOVES
	OnMouseMoved(mouse/Mouse)
		src << "client.OnMouseMoved(Mouse)"
		..()

	OnMouseDragAndDrop(mouse/Mouse, Button, Modifier)
		src << "client.OnMouseDragAndDrop(Mouse, [Button], [Modifier])"
		..()
	#endif

	OnMousePressed(mouse/Mouse, Button, Modifier)
		src << "client.OnMousePressed(Mouse, [Button], [Modifier])"
		..()

	OnMouseReleased(mouse/Mouse, Button, Modifier)
		src << "client.OnMouseReleased(Mouse, [Button], [Modifier])"
		..()

	OnMouseClicked(mouse/Mouse, Button, Modifier)
		src << "client.OnMouseClicked(Mouse, [Button], [Modifier])"
		..()

mob/player
	icon_state = "arrow"
	color = "navy"
	step_size = 4

	var
		moving_camera = FALSE

		last_draw_x
		last_draw_y

	Login()
		..()

		#ifdef MOUSE_ENABLE_EVENTS
		// Hook up some nice events to whatever procs we want.
		// They'll be called automatically when the event fires!
		// See the definitions to find out what arguments are passed to callbacks.
		client.mouse.OnPressed.Add(src, .proc/HandleMousePressed)
		client.mouse.OnReleased.Add(src, .proc/HandleMouseReleased)

		#ifndef MOUSE_DISABLE_MOVES
		client.mouse.OnMoved.Add(src, .proc/HandleMouseMoved)
		#endif

		#endif

		// do something every frame
		spawn for()
			sleep world.tick_lag

			// if moving camera mode is enabled, move the camera around
			if(moving_camera)
				client.pixel_x = 128 * cos(world.time * 10)
				client.pixel_y = 96 * sin(world.time * 27)

			#ifndef MOUSE_DISABLE_MOVES
			// face the mouse if it's on the map
			if(client.mouse.is_tracked)
				transform = turn(initial(transform), client.mouse.GetAngle())

			// draw a dot on the map with the right mouse button
			// if the mouse moved (relative to the map)
			if(client.mouse.buttons & MOUSE_RIGHT_BUTTON)
				var draw_x = client.mouse.GetMapPixelX()
				var draw_y = client.mouse.GetMapPixelY()
				if(draw_x != last_draw_x || draw_y != last_draw_y)
					last_draw_x = draw_x
					last_draw_y = draw_y
					new /obj/dot (draw_x, draw_y, z)
			#endif

	#ifdef MOUSE_ENABLE_EVENTS
	proc
		HandleMousePressed(mouse/Mouse, Button, Modifier)
			src << "Event: [MouseDumpLink()] pressed with button [Button] and modifier [Modifier]"

		HandleMouseReleased(mouse/Mouse, Button, Modifier)
			src << "Event: [MouseDumpLink()] released with button [Button] and modifier [Modifier]"

		#ifndef MOUSE_DISABLE_MOVES
		HandleMouseMoved(mouse/Mouse)
			src << "Event: [MouseDumpLink()] moved"

		HandleMouseDragAndDrop(mouse/Mouse, Button, Modifier)
			src << "Event: [MouseDumpLink()] drag and drop with button [Button] and modifier [Modifier]"
		#endif

		// this was fun.
		MouseDumpLink()
			if(!mouse_dump) mouse_dump = new
			var html = "<head><title>Mouse Dump [++mouse_dump.len]</title></head><tt>"

			#ifndef MOUSE_DISABLE_MOVES
			var global/plain_vars[] = list(
				"map_id", "move_x", "move_y", "screen_loc",
				"screen_pixel_x", "screen_pixel_y",
				"screen_step_x", "screen_step_y",
				"screen_tile_x", "screen_tile_y"
				)
			#endif

			var global/references[] = list("client", "over_object"
			#ifndef MOUSE_DISABLE_MOVES
			, "drag_object"
			#endif
			)

			#ifndef MOUSE_DISABLE_MOVES
			var global/bools[] = list("is_tracked")
			#endif

			for(var/reference in references)
				var value = client.mouse.vars[reference]
				html += "[reference] = \ref[value] [value]"
				if(istype(value, /datum)) html += " ([value:type])"
				html += "<br>"

			if(!client.mouse.buttons)
				html += "buttons = 0 (None)<br>"
			else
				var buttons[0]
				if(client.mouse.buttons & MOUSE_LEFT_BUTTON) buttons += "Left"
				if(client.mouse.buttons & MOUSE_RIGHT_BUTTON) buttons += "Right"
				if(client.mouse.buttons & MOUSE_MIDDLE_BUTTON) buttons += "Middle"
				html += "buttons = [client.mouse.buttons] [json_encode(buttons)]<br>"

			#ifndef MOUSE_DISABLE_MOVES
			for(var/plain_var in plain_vars)
				var value = client.mouse.vars[plain_var]
				if(istext(value))
					html += "[plain_var] = \"[value]\"<br>"
				else
					html += "[plain_var] = [value]<br>"

			for(var/bool in bools)
				var value = client.mouse.vars[bool]
				html += "[bool] = "
				html += value ? "TRUE (1)" : "FALSE (0)"
				html += "<br>"
			#endif

			html += "params = [json_encode(client.mouse.params)]<br>"

			mouse_dump[mouse_dump.len] = html
			return "<a href=?src=\ref[src];action=mouse+dump;index=[mouse_dump.len]>Mouse</a>"

	var
		mouse_dump[]

	Topic(href, href_list[])
		if(href_list["action"] == "mouse dump")
			src << browse(mouse_dump[text2num(href_list["index"])], "window=mouse_dump_[href_list["index"]]")

	#endif

	verb
		moving_camera()
			moving_camera = !moving_camera

			if(!moving_camera)
				client.pixel_x = 0
				client.pixel_y = 0

		set_view(new_view as text)
			client.view = text2num(new_view) || new_view

	Stat()
		statpanel("Mouse")

		if(!client.mouse.buttons)
			stat("Buttons", "0 (None)")
		else
			var buttons[0]
			if(client.mouse.buttons & MOUSE_LEFT_BUTTON) buttons += "Left"
			if(client.mouse.buttons & MOUSE_RIGHT_BUTTON) buttons += "Right"
			if(client.mouse.buttons & MOUSE_MIDDLE_BUTTON) buttons += "Middle"
			stat("Buttons", "[client.mouse.buttons] [json_encode(buttons)]")

		stat("Over Object", client.mouse.over_object)

		#ifndef MOUSE_DISABLE_MOVES

		stat("Drag Object", client.mouse.drag_object)

		stat("Map ID", client.mouse.map_id)
		stat("Is On Map", client.mouse.is_tracked ? "TRUE (1)" : "FALSE (0)")

		stat("Screen Loc", client.mouse.screen_loc)
		stat("Screen Tile", "([client.mouse.screen_tile_x], [client.mouse.screen_tile_y])")
		stat("Screen Step", "([client.mouse.screen_step_x], [client.mouse.screen_step_y])")
		stat("Screen Pixel", "([client.mouse.screen_pixel_x], [client.mouse.screen_pixel_y])")
		stat("Map Pixel", "([client.mouse.GetMapPixelX()], [client.mouse.GetMapPixelY()])")

		stat("Delta", "([client.mouse.GetDeltaX()], [client.mouse.GetDeltaY()])")
		stat("Angle", client.mouse.GetAngle())

		stat("Move", "([client.mouse.move_x], [client.mouse.move_y])")
		#endif

obj/dot
	icon_state = "rect"
	transform = matrix(1/32, 0, -15.5, 0, 1/32, -15.5)

	New(Px, Py, Z)
		loc = locate(1 + (Px - 1) / 32, 1 + (Py - 1) / 32, Z)
		step_x = (Px - 1) % 32
		step_y = (Py - 1) % 32
