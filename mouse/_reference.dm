#if 0
/*

	All you need to know to use this library!

*/

/*
How to use pre-processor macros:
Add these #defines to your own .dme file before the BEGIN_INCLUDE line.
(so that it's above the #include for this library.)
*/

/*
Removes mouse events using the Kaiochao.Event library.
*/
#define MOUSE_DISABLE_EVENTS

// Here are some flags that let you disable "extra communcation overhead".

/*
Removes everything related to mouse position tracking, including client hooks
that are called whenever the mouse moves. Turns this library into a
mouse-button-tracking library.
Saves bandwidth and a bit of server processing.
*/
#define MOUSE_DISABLE_MOVES

/*
Removes all the client mouse hooks built into the library.
This lets you define your own hooks per-object.
Make sure you follow how the hooks are defined at the bottom of Mouse.dm.
Use usr.client.mouse as the mouse involved.
*/
#define MOUSE_DISABLE_CLIENT_HOOKS

/*
Removes the MouseButton and MouseModifier enumeration types.
*/
#define MOUSE_DISABLE_ENUMS

// Attached mouse
client/var/mouse/mouse

// Global constants:
var/const

	// Mouse flags:

	// Mouse buttons (bit flags)
	MOUSE_LEFT_BUTTON	= 1
	MOUSE_RIGHT_BUTTON	= 2
	MOUSE_MIDDLE_BUTTON	= 4

	// Mouse modifier keys (bit flags, can be combined with mouse buttons)
	MOUSE_CTRL_KEY		= 8
	MOUSE_SHIFT_KEY		= 16
	MOUSE_ALT_KEY		= 32

// Enumerations for the global constants:
ENUM(MouseButton)
	None = 0
	Left = 1
	Right = 2
	Middle = 4

ENUM(MouseModifier)
	None = 0
	Control = 8
	Shift = 16
	Alt = 32

// Both /atom and /client have these procs, with the same args.
// The client proc calls the atom proc, just like in the built-in mouse procs,
// so be sure to call ..() in the client proc if you want the atom proc to call.
client/proc
atom/proc
	// Called when any mouse button is pressed while the mouse is over this object.
	OnMousePressed(mouse/Mouse, MouseFlags)

	// Called when any mouse button is released while the mouse is over this object.
	OnMouseReleased(mouse/Mouse, MouseFlags)

	OnMouseClick(mouse/Mouse, MouseFlags)

	// Called when:
	// no mouse buttons are pressed,
	// then a mouse button is pressed while the mouse is over this object,
	// then a mouse button is released while the mouse is over a different object.
	OnDragAndDrop(mouse/Mouse, MouseFlags)

mouse/proc
	// The pixel coordinate of the mouse on the map with (1, 1) being the bottom-left pixel.
	GetMapPixelX()
	GetMapPixelY()

	// The distance in pixels along the x or y axis from From to the mouse cursor.
	GetDeltaX(atom/From)
	GetDeltaY(atom/From)

	// The angle (degrees clockwise from 0 = NORTH) from From to the mouse cursor.
	GetAngle(atom/From)

mouse/var
	global
		// The obj that's added to all clients' screens to catch the mouse in places with no other atoms.
		obj/mouse_catcher/mouse_catcher

		// The regex that parses the screen-loc parameter of mouse events into its parts.
		regex/screen_loc_regex

	// The client this mouse is attached to.
	client/client

	atom/over_object	// The atom currently under the mouse pointer.
	atom/drag_object	// The atom being dragged, if any.

	// If MOUSE_ENABLE_EVENTS is defined, you get some nice events (from the Kaiochao.Event library)
	event
		// Fires when the mouse moves. Only exists without MOUSE_DISABLE_MOVES.
		OnMoved(mouse/Mouse)

		// Fires when one of the mouse's buttons is pressed.
		OnPressed(mouse/Mouse, MouseFlags)

		// Fires when one of the mouse's buttons is released.
		OnReleased(mouse/Mouse, MouseFlags)

		// Fires when a mouse button is pressed then a mouse button is released, over the same object.
		OnClicked(mouse/Mouse, MouseFlags)

		// Fires when dragging and dropping an object onto another object.
		OnDragAndDrop(mouse/Mouse, MouseFlags)

	// One of the MouseButtons constants.
	buttons

	// The last list of parameters received by the client mouse events.
	params[]

	// The screen-loc parameter from params.
	screen_loc

	// ID of the map the mouse was last known to be in.
	map_id

	// TRUE if the mouse is currently being tracked.
	is_tracked

	// The pixel coordinate of the mouse in the screen, bottom-left is (1, 1).
	screen_pixel_x
	screen_pixel_y

	// The tile coordinate of the mouse in the screen from screen-loc.
	screen_tile_x
	screen_tile_y

	// The sub-tile "step" coordinate of the mouse in the screen from screen-loc.
	screen_step_x
	screen_step_y

	// If MOUSE_DISABLE_MOVES is not defined, this contains the last movement of the mouse, in pixels.
	// The movement is the difference from the latest screen position to the position before that
	move_x
	move_y
#endif
