#if 0
/*

=== July 14, 2016 ===

CHANGED mouse events to be enabled by default.

=== July 10, 2016 ===

By the way, this library sets client.show_popup_menus to FALSE by default.
If you want your popup menus back, you'll have to set it to TRUE in your own project, but... why would you do that?

Also, I did a bit of reorganizing.

ADDED MOUSE_DISABLE_CLIENT_HOOKS flag to exclude client hooks so you can choose which objects will interact with the mouse.

ADDED Enums: MouseButton and MouseModifier
	The have the same values as the MOUSE_* constants, but they're accessed as
	enumerations using the Kaiochao.Enum library.

	Can be excluded with the MOUSE_DISABLE_ENUMS flag.

CHANGED the MOUSE_DISABLE_MOVES flag to disable a ton more stuff
	When MOUSE_DISABLE_MOVES is defined (which of course it isn't by default),
	everything that relies on mouse movement is disabled and unaccessible.
	This is because client.MouseMove(), MouseDrag(), Entered(), and Exited() are
	no longer overridden, and anything that relies on those to work, can't work.
	The benefit here is lower bandwidth usage and less server processing.

=== July 9, 2016 ===

REPLACED (MouseFlags) with (Button, Modifier)
MouseFlags is too annoying to deal with!

=== July 8, 2016 ===

I've discovered that stddef.dm contains contants for mouse buttons and modifiers already.
...It's built-in, so I might as well use it, right?

REMOVED dependency on Kaiochao.Enum
REPLACED MouseButtons and MouseModifiers with constants e.g. MOUSE_LEFT_BUTTON, MOUSE_CTRL_KEY (see _reference.dm)
REPLACED (mouse/Mouse, Button, Modifier) with (mouse/Mouse, MouseFlags) for all the mouse events

ADDED client.OnMouseX(mouse/Mouse, MouseFlags), which calls atom.OnMouseX(Mouse, MouseFlags).

=== July 7 2016 ===

Apparently, if you're dragging an object with one mouse button,
and you click a different object with a different mouse button,
then the 'object' in client.MouseDown(object, ...) is the dragged object,
not the object that was just clicked. WEIRD

FIXED client.MouseExited() caused reports of weird twitchy mouse movements.

ADDED _reference.dm
	Check it. NOW!

CHANGED changelog.dm to _changelog.dm
	You might still have changelog.dm in your folder, so go ahead and delete it.

ADDED mouse.over_object and mouse.drag_object

ADDED atom.OnMousePressed, OnMouseReleased, OnDragAndDrop, and OnMouseClicked(mouse/Mouse, MouseFlags)
ADDED events: mouse.OnClicked and OnDragAndDrop(mouse/Mouse, MouseFlags).

ADDED "atom/From" argument to mouse.GetDeltaX(), .GetDeltaY(), and .GetAngle().

=== July 6 2016 ===

Minimum BYOND version is now 510

CHANGED the mouse to be a datum, client.mouse.
	Sorry about that! It should just take some Find/Replace to fix.

CHANGED mouse.buttons to use the MouseButtons enumerator

FIXED the mouse catcher being layered wrongly in BYOND 510
	CHANGED mouse catcher's plane to -100 and layer to BACKGROUND_LAYER

REPLACED client.mouse_map_x/y with mouse.GetMapPixelX/Y()

ADDED support for events using the Kaiochao.Event library
	ADDED OnPressed, OnMoved, and OnReleased events to the mouse
	NOTE must be enabled with the MOUSE_ENABLE_EVENTS flag because it relies on another library

ADDED mouse.move_x/y, which tells you how many pixels the mouse has moved since the last movement
	NOTE can be disabled with the MOUSE_DISABLE_MOVES flag

ADDED the mouse catcher as its own type, /mouse_catcher

IMPROVED screen-loc parsing to use regex
	ADDED mouse.screen_loc_regex, which contains the regex used to parse screen-locs
	NOTE map IDs are required to not be a string of digits, to avoid parser confusion
		BAD: "12345"
		GOOD: "map" "map1" "25137580a20951627"

ADDED changelog.dm

*/
#endif
