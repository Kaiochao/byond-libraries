#ifndef TILE_WIDTH
#define TILE_WIDTH 32
#endif

#ifndef TILE_HEIGHT
#define TILE_HEIGHT 32
#endif

// Detects the mouse where there are no other atoms.
obj/mouse_catcher
	name = ""
	plane = -100
	layer = BACKGROUND_LAYER
	mouse_opacity = 2
	screen_loc = "SOUTHWEST to NORTHEAST"

#ifndef MOUSE_DISABLE_EVENTS
#include <kaiochao\event\Event.dme>
#endif

#ifndef MOUSE_DISABLE_ENUMS
#include <kaiochao\enum\Enum.dme>

ENUM(MouseButton)
	None = 0
	Left = 1
	Right = 2
	Middle = 4

ENUM(MouseModifier)
	None = 0
	Control = 8
	Shift = 16
	Alt = 32
#endif

#ifndef MOUSE_DISABLE_CLIENT_EVENTS

atom/proc
	OnMousePressed(mouse/Mouse, Button, Modifier)
	OnMouseClicked(mouse/Mouse, Button, Modifier)
	OnMouseReleased(mouse/Mouse, Button, Modifier)
	OnDragAndDrop(mouse/Mouse, Button, Modifier)

#endif

mouse
	New(client/Client)
		client = Client
		Client.screen += mouse_catcher

mouse/var/global
	regex/screen_loc_regex = regex("^(?:(?!\[0-9]+:)(\\w+):)?(\\d+)(?::(\\d+))?,(\\d+)(?::(\\d+))?$")
	obj/mouse_catcher/mouse_catcher = new

mouse/var
	client/client

	params[]

	buttons = 0

	atom/over_object

	#ifndef MOUSE_DISABLE_MOVES

	map_id = ":map"

	is_tracked = FALSE

	atom/drag_object

	screen_loc = ""

	screen_tile_x = 0
	screen_tile_y = 0

	screen_step_x = 0
	screen_step_y = 0

	screen_pixel_x = 0
	screen_pixel_y = 0

	move_x = 0
	move_y = 0
	#endif

#ifndef MOUSE_DISABLE_EVENTS
mouse/var/tmp/event
	OnPressed	// (mouse/Mouse, Button, Modifier)
	OnReleased	// (mouse/Mouse, Button, Modifier)
	OnClicked	// (mouse/Mouse, Button, Modifier)

	#ifndef MOUSE_DISABLE_MOVES
	OnMoved			// (mouse/Mouse)
	OnDragAndDrop	// (mouse/Mouse, Button, Modifier)
	#endif
#endif

mouse/proc
	#ifndef MOUSE_DISABLE_MOVES
	GetMapPixelX() return client.bound_x - 1 + screen_pixel_x
	GetMapPixelY() return client.bound_y - 1 + screen_pixel_y

	GetDeltaX(atom/From)
		From = From || client.eye
		. = GetMapPixelX() - ((istype(From, /atom/movable) ? (From:bound_x + From:step_x + From:bound_width  * 0.5) : (TILE_WIDTH/2))  + (From.x - 1) * TILE_WIDTH + 1)

	GetDeltaY(atom/From)
		From = From || client.eye
		. = GetMapPixelY() - ((istype(From, /atom/movable) ? (From:bound_y + From:step_y + From:bound_height * 0.5) : (TILE_HEIGHT/2)) + (From.y - 1) * TILE_HEIGHT + 1)

	GetAngle(atom/From)
		From = From || client.eye
		// this is just atan2, nothing special
		var dx = GetDeltaX(From), dy = GetDeltaY(From)
		. = (dx || dy) && (dx >= 0 \
			? arccos(dy / sqrt(dx * dx + dy * dy)) \
			: -arccos(dy / sqrt(dx * dx + dy * dy)))
	#endif

	#ifndef MOUSE_DISABLE_EVENTS

	#ifndef MOUSE_DISABLE_MOVES
	OnMoved() OnMoved && OnMoved.Fire(src)
	OnDragAndDrop(Button, Modifier) OnDragAndDrop && OnDragAndDrop.Fire(src, Button, Modifier)
	#endif

	OnPressed(Button, Modifier) OnPressed && OnPressed.Fire(src, Button, Modifier)
	OnReleased(Button, Modifier) OnReleased && OnReleased.Fire(src, Button, Modifier)
	OnClicked(Button, Modifier) OnClicked && OnClicked.Fire(src, Button, Modifier)

	#else

	#ifndef MOUSE_DISABLE_MOVES
	OnMoved() client.OnMouseMoved(src)
	OnDragAndDrop(Button, Modifier) client.OnMouseDragAndDrop(src, Button, Modifier)
	#endif

	OnPressed(Button, Modifier) client.OnMousePressed(src, Button, Modifier)
	OnReleased(Button, Modifier) client.OnMouseReleased(src, Button, Modifier)
	OnClicked(Button, Modifier) client.OnMouseClicked(src, Button, Modifier)
	#endif

	/*
		Called by the built-in client mouse events.
		OverObject: The "object" parameter of the client events.
			Refers to the object currently under the mouse pointer.
		Params: The "params" parameter of the client events, converted to a list
			using params2list().
		IsReleased: TRUE when the release of a mouse button caused a call to
			this. FALSE otherwise.
		Moved: TRUE when a mouse movement called this. FALSE otherwise.
	*/
	HandleMouseEvent(atom/OverObject, Params[], IsReleased, Moved, Exited)
		params = Params

		var button = 0
		if(Params["left"]) button = MOUSE_LEFT_BUTTON
		else if(Params["right"]) button = MOUSE_RIGHT_BUTTON
		else if(Params["middle"]) button = MOUSE_MIDDLE_BUTTON

		var modifier = 0
		if(Params["ctrl"]) modifier = MOUSE_CTRL_KEY
		else if(Params["alt"]) modifier = MOUSE_ALT_KEY
		else if(Params["shift"]) modifier = MOUSE_SHIFT_KEY

		#ifdef MOUSE_DISABLE_MOVES
		if(IsReleased)
			buttons &= ~button
			OnReleased(button, modifier)
			if(!buttons) over_object = null
		else
			over_object = OverObject
			buttons |= button
			OnPressed(button, modifier)
		#else

		if(!(Moved || Exited))
			over_object = OverObject
			if(IsReleased)
				buttons &= ~button
				OnReleased(button, modifier)
				drag_object && drag_object != over_object \
				? OnDragAndDrop(button, modifier) \
				: OnClicked(button, modifier)
				drag_object = null

			else
				buttons |= button
				drag_object = over_object
				OnPressed(button, modifier)

		else
			var ScreenLoc = Params["screen-loc"]

			if(ScreenLoc && screen_loc != ScreenLoc)
				screen_loc = ScreenLoc

				screen_loc_regex.Find(ScreenLoc)
				var data[] = screen_loc_regex.group
				map_id = data[1] || map_id
				screen_tile_x = text2num(data[2])
				screen_step_x = text2num(data[3])
				screen_tile_y = text2num(data[4])
				screen_step_y = text2num(data[5])

				var previous_screen_pixel_x = screen_pixel_x
				var previous_screen_pixel_y = screen_pixel_y

				screen_pixel_x = screen_step_x + (screen_tile_x-1) * TILE_WIDTH
				screen_pixel_y = screen_step_y + (screen_tile_y-1) * TILE_HEIGHT

				move_x = screen_pixel_x - previous_screen_pixel_x
				move_y = screen_pixel_y - previous_screen_pixel_y

				previous_screen_pixel_x = screen_pixel_x
				previous_screen_pixel_y = screen_pixel_y

				(move_x || move_y) && OnMoved()
		#endif

client
	show_popup_menus = FALSE

	var tmp/mouse/mouse

	New()
		mouse = new (src)

		#ifndef MOUSE_DISABLE_EVENTS
		#ifndef MOUSE_DISABLE_CLIENT_EVENTS
		EVENT_ADD(mouse.OnPressed, src, .proc/OnMousePressed)
		EVENT_ADD(mouse.OnReleased, src, .proc/OnMouseReleased)
		EVENT_ADD(mouse.OnClicked, src, .proc/OnMouseClicked)

		#ifndef MOUSE_DISABLE_MOVES
		EVENT_ADD(mouse.OnMoved, src, .proc/OnMouseMoved)
		EVENT_ADD(mouse.OnDragAndDrop, src, .proc/OnMouseDragAndDrop)
		#endif
		#endif
		#endif

		. = ..()

	#ifndef MOUSE_DISABLE_CLIENT_EVENTS
	proc
		#ifndef MOUSE_DISABLE_MOVES
		OnMouseMoved(mouse/Mouse)

		OnMouseDragAndDrop(mouse/Mouse, Button, Modifier)
			Mouse.drag_object && Mouse.drag_object.OnDragAndDrop(Mouse, Button, Modifier)
		#endif

		OnMouseReleased(mouse/Mouse, Button, Modifier)
			#ifndef MOUSE_DISABLE_MOVES
			Mouse.drag_object && Mouse.drag_object.OnMouseReleased(Mouse, Button, Modifier)
			#else
			Mouse.over_object && Mouse.over_object.OnMouseReleased(Mouse, Button, Modifier)
			#endif

		OnMousePressed(mouse/Mouse, Button, Modifier)
			Mouse.over_object && Mouse.over_object.OnMousePressed(Mouse, Button, Modifier)

		OnMouseClicked(mouse/Mouse, Button, Modifier)
			Mouse.over_object && Mouse.over_object.OnMouseClicked(Mouse, Button, Modifier)
	#endif

	// The idea is to track all movements and button presses and releases of the
	// mouse. To do so, we have to pretty much define all mouse events.

	#ifndef MOUSE_DISABLE_CLIENT_HOOKS

	MouseDown(object, location, control, params)
		mouse.HandleMouseEvent(object, params2list(params))
		..()

	// Luckily, this is called when MouseDrop() is called, or I'd need to
	// override that one too.
	MouseUp(object, location, control, params)
		mouse.HandleMouseEvent(object, params2list(params), IsReleased = TRUE)
		..()

	#ifndef MOUSE_DISABLE_MOVES
	// This (and MouseExited()) is called when the mouse changes from one object
	// (if any) to another. MouseMove() and MouseDrag() are not called.
	MouseEntered(object, location, control, params)
		mouse.is_tracked = TRUE
		mouse.HandleMouseEvent(object, params2list(params), Moved = TRUE)
		..()

	// This isn't used to track the mouse's position because it doesn't actually
	// report the mouse's current position, it reports the position it was at
	// when it MouseEntered() [object]. Weird.
	// What this IS used for: setting mouse.is_tracked, which lets the user know
	// when the mouse is off the map.
	// MouseMove() and MouseDrag() are not called, but MouseEntered() will be
	// if there's a new object.
	MouseExited(object, location, control, params)
		..()
		mouse.HandleMouseEvent(object, params2list(params), Exited = TRUE)
		mouse.is_tracked = FALSE

	// If MOUSE_DISABLE_MOVES is defined, this is only meant to track
	// over_object while a mouse button is pressed.
	// Mouse position tracking when at least one mouse button is being pressed.
	MouseDrag(src_object, over_object, src_location, over_location, src_control, over_control, params)
		mouse.HandleMouseEvent(over_object, params2list(params), Moved = TRUE)
		..()

	// Pixel-precise mouse position tracking in MouseDrag()
	// requires the definition of the MouseMove() proc, which fires when the
	// mouse moves within the same object.
	// It isn't actually pixel precise if [object] is scaled up using
	// atom.transform or "map.zoom" because the pixels the mouse sees are bigger
	// than the pixels on your screen.
	// This isn't called if any mouse buttons are being pressed.
	MouseMove(object, location, control, params)
		mouse.HandleMouseEvent(object, params2list(params), Moved = TRUE)
		..()

	#endif

	#endif
